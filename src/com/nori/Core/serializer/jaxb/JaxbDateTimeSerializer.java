package com.nori.Core.serializer.jaxb;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.nori.Core.utility.StringUtil;

public class JaxbDateTimeSerializer extends XmlAdapter<String, Date> {

	@Override
	public String marshal(Date date) throws Exception {
		if (date == null) {
			return "";
		}
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		return formatter.format(date);
	}
	
	@Override
	public Date unmarshal(String date) throws Exception {
		if (StringUtil.isEmptyString(date)) {
			return null;
		}
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		return formatter.parse(date);
	}
}
