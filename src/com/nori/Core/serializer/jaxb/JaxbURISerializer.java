package com.nori.Core.serializer.jaxb;

import java.net.URI;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.nori.Core.utility.StringUtil;

public class JaxbURISerializer extends XmlAdapter<String, URI> {

	@Override
	public String marshal(URI uri) throws Exception {
		if (uri == null) {
			return "";
		}
		return uri.toString();
	}
	
	@Override
	public URI unmarshal(String uri) throws Exception {
		if (StringUtil.isEmptyString(uri)) {
			return null;
		}
		return new URI(uri);
	}
}

