package com.nori.Core.serializer.jaxb;

import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.nori.Core.utility.StringUtil;

public class JaxbTwoFractionTimeSerializer extends XmlAdapter<String, Date> {

	private static final DateTimeFormatter formatter = DateTimeFormat.forPattern("HH:mm:ss:SS");
	
	@Override
	public String marshal(Date date) throws Exception {
		if (date == null) {
			return "";
		}
		//DateTimeFormatter formatter = DateTimeFormat.forPattern("HH:mm:ss:SS");
		return formatter.print(date.getTime());
	}

	@Override
	public Date unmarshal(String date) throws Exception {
		if (StringUtil.isEmptyString(date)) {
			return null;
		}
		//DateTimeFormatter formatter = DateTimeFormat.forPattern("HH:mm:ss:SS");
		DateTime dateTime = formatter.parseDateTime(date);
		return dateTime.toDate();
	}
}
