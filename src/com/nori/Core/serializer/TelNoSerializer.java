package com.nori.Core.serializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.nori.Core.utility.StringUtil;

public class TelNoSerializer extends JsonSerializer<String> {

	@Override
	public void serialize(String phone, JsonGenerator gen, SerializerProvider provider) throws IOException, JsonProcessingException {

		if (phone == null) {
			gen.writeNull();
		} else {
			gen.writeString(StringUtil.koreanTelNo(phone));
		}
	}
}