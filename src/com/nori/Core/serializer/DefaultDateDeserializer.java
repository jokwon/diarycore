package com.nori.Core.serializer;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.nori.Core.utility.StringUtil;

public class DefaultDateDeserializer extends JsonDeserializer<Date> {
	
	@Override
	public Date deserialize(JsonParser jsonparser, DeserializationContext deserializationcontext) throws IOException, JsonProcessingException {

		try {
			SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM dd yyyy HH:mm:ss 'GMT'Z", Locale.US);
			String date = jsonparser.getText();
			if (!StringUtil.isEmptyString(date)) {
				return formatter.parse(date);
			}
			return null;
		} catch (Exception ex) {
			return null;
		}
	}
}