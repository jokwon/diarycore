package com.nori.Core.serializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.nori.Core.utility.StringUtil;

public class PostalSerializer extends JsonSerializer<String> {

	@Override
	public void serialize(String postal, JsonGenerator gen, SerializerProvider provider) throws IOException, JsonProcessingException {

		if (postal == null) {
			gen.writeNull();
		} else {
			gen.writeString(StringUtil.zip(postal));
		}
	}
}
