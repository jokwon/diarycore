package com.nori.Core.tags;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import javax.servlet.ServletContext;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.tools.generic.DateTool;
import org.apache.velocity.tools.generic.NumberTool;
import org.springframework.ui.velocity.VelocityEngineUtils;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.nori.Core.utility.DateUtil;
import com.nori.Core.utility.StringUtil;
import com.nori.Core.form.Pagination;

public class PaginationTag extends TagSupport {

	private static final long serialVersionUID = 1L;

	private String form;
	private String pageNo;
	private String pageSize;
	private String target;
	private String method;
	private String prefixPageNoName;
	private String function;
	private String url;
	private String encoding;
	private String template;
	private Pagination page;
	private boolean absolute;
	private boolean tools;

	public String getForm() {
		return form;
	}

	public void setForm(String form) {
		this.form = form;
	}

	public String getPageNo() {
		return pageNo;
	}

	public void setPageNo(String pageNo) {
		this.pageNo = pageNo;
	}

	public String getPageSize() {
		return pageSize;
	}

	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public Pagination getPage() {
		return page;
	}

	public void setPage(Pagination page) {
		this.page = page;
	}

	public boolean isAbsolute() {
		return absolute;
	}

	public void setAbsolute(boolean absolute) {
		this.absolute = absolute;
	}

	public boolean isTools() {
		return tools;
	}

	public void setTools(boolean tools) {
		this.tools = tools;
	}
	
	public String getPrefixPageNoName() {
		return prefixPageNoName;
	}

	public void setPrefixPageNoName(String prefixPageNoName) {
		this.prefixPageNoName = prefixPageNoName;
	}

	public int doStartTag() throws JspException {
		if (pageContext.getRequest() != null) {
			ServletContext servletContext = pageContext.getServletContext();
			if (page != null && servletContext != null) {
				if (servletContext.getAttribute("__fz_pagination_number__") == null) {
					servletContext.setAttribute("__fz_pagination_number__", new AtomicLong());
				}
				long uid = ((AtomicLong) servletContext.getAttribute("__fz_pagination_number__")).incrementAndGet();
				WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(servletContext);
				if (ctx != null) {
					VelocityEngine velocityEngine = ctx.getBean(VelocityEngine.class);
					if (velocityEngine != null) {
						if (StringUtil.isEmptyString(form)) {
							form = "theForm";
						}

						if (StringUtil.isEmptyString(pageNo)) {
							pageNo = "pageNo";
						}

						if (StringUtil.isEmptyString(pageSize)) {
							pageSize = "pageSize";
						}

						if (StringUtil.isEmptyString(target)) {
							target = "_self";
						}

						if (StringUtil.isEmptyString(method)) {
							method = "post";
						}

						if (StringUtil.isEmptyString(function)) {
							function = "goPage";
						}

						if (StringUtil.isEmptyString(encoding)) {
							encoding = "UTF-8";
						}

						if (StringUtil.isEmptyString(template)) {
							template = "com/nori/Core/tags/pagination.vsl";
						} else if (template.equalsIgnoreCase("pagination")) {
							template = "com/nori/Core/tags/pagination.vsl";
						} else if (template.equalsIgnoreCase("summary")) {
							template = "com/nori/Core/tags/summary.vsl";
						}

						JspWriter out = pageContext.getOut();

						Map<String, Object> model = new HashMap<String, Object>();
						model.put("page", page);
						model.put("function", function);
						model.put("url", url);
						if (absolute) {
							model.put("contextPath", "");
						} else {
							model.put("contextPath", servletContext.getContextPath());
						}
						model.put("form", form);
						model.put("prefixPageNoName", prefixPageNoName);
						model.put("pageNo", pageNo);
						model.put("pageSize", pageSize);
						model.put("target", target);
						model.put("method", method);
						model.put("uid", uid);
						if (tools) {
							model.put("dateTool", new DateTool());
							model.put("numberTool", new NumberTool());
							model.put("StringUtil", StringUtil.class);
							model.put("DateUtil", DateUtil.class);
						}
						VelocityEngineUtils.mergeTemplate(velocityEngine, template, encoding, model, out);
					}
				}
			}
		}
		return SKIP_BODY;
	}

	
}
