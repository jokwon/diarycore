package com.nori.Core.tags;

public class ForEachStatus {

	private Object current;
	private int index;
	private int count;
	private boolean first;
	private boolean last;
	private int begin;
	private int end;
	private int step;

	public ForEachStatus() {
		this.step = 1;
	}

	public int getBegin() {
		return begin;
	}

	protected void setBegin(int begin) {
		this.begin = begin;
	}

	public int getCount() {
		return count;
	}

	protected void setCount(int count) {
		this.count = count;
	}

	public Object getCurrent() {
		return current;
	}

	protected void setCurrent(Object current) {
		this.current = current;
	}

	public int getEnd() {
		return end;
	}

	protected void setEnd(int end) {
		this.end = end;
	}

	public boolean isFirst() {
		return first;
	}

	protected void setFirst(boolean first) {
		this.first = first;
	}

	public int getIndex() {
		return index;
	}

	protected void setIndex(int index) {
		this.index = index;
	}

	public boolean isLast() {
		return last;
	}

	protected void setLast(boolean last) {
		this.last = last;
	}

	public int getStep() {
		return step;
	}

	protected void setStep(int step) {
		this.step = step;
	}
}
