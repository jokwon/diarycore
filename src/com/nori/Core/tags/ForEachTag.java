package com.nori.Core.tags;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class ForEachTag extends SimpleTagSupport {

	private static final long serialVersionUID = 1L;
	private int begin;
	private int end;
	private String step;
	private Object items;
	private String var;
	private String varStatus;
	private boolean reverse;
	private boolean checkStatus;
	private boolean checkBreak;
	private String breakConditionName;
	//private int scope = PageContext.REQUEST_SCOPE;

	public void setBegin(int begin) {
		this.begin = begin;
	}

	public void setEnd(int end) {
		this.end = end;
	}

	public void setItems(Object items) {
		this.items = items;
	}

	public void setStep(String step) {
		this.step = step;
	}

	public void setVar(String var) {
		this.var = var;
	}

	public void setVarStatus(String varStatus) {
		this.varStatus = varStatus;
	}

	public void setReverse(boolean reverse) {
		this.reverse = reverse;
	}

	public void setBreak(String breakConditionName) {
		this.breakConditionName = breakConditionName;
	}

	@Override
	public void doTag() throws JspException, IOException {
		ForEachStatus status = new ForEachStatus();
		try {
			status.setStep(Integer.parseInt(step));
		} catch (Exception ex) {
			status.setStep(1);
		}
		if (status.getStep() <= 0) {
			status.setStep(1);
		}
		if (varStatus == null || "".equals(varStatus.trim())) {
			varStatus = "";
		}
		if (!"".equals(varStatus)) {
			checkStatus = true;
		}
		if (breakConditionName == null || "".equals(breakConditionName.trim())) {
			breakConditionName = "";
		}
		if (!"".equals(breakConditionName)) {
			checkBreak = true;
		}
		int count = 1;
		if (items != null) {
			if (items instanceof List) {
				List target = (List) items;
				Object[] values = target.toArray();
				if (reverse) {
					for (int i = values.length - 1, len = values.length - 1; i >= 0; i--) {
						if (checkBreak) {
							Object breakValue = (Object) getJspContext().getAttribute(breakConditionName);
							if (breakValue != null && Boolean.valueOf(breakValue.toString()).booleanValue() == true) {
								break;
							}
						}
						getJspContext().setAttribute(var, values[i]);
						if (checkStatus) {
							if (i == len) {
								status.setFirst(true);
							} else {
								status.setFirst(false);
							}
							if (i == 0) {
								status.setLast(true);
							} else {
								status.setLast(false);
							}
							status.setBegin(len);
							status.setEnd(0);
							status.setCurrent(values[i]);
							status.setIndex(i);
							status.setCount(count);
							getJspContext().setAttribute(varStatus, status);
						}
						getJspBody().invoke(null);
						count++;
					}
				} else {
					for (int i = 0, len = values.length, endidx = values.length - 1; i < len; i++) {
						if (checkBreak) {
							Object breakValue = (Object) getJspContext().getAttribute(breakConditionName);
							if (breakValue != null && Boolean.valueOf(breakValue.toString()).booleanValue() == true) {
								break;
							}
						}
						getJspContext().setAttribute(var, values[i]);
						if (checkStatus) {
							if (i == 0) {
								status.setFirst(true);
							} else {
								status.setFirst(false);
							}
							if (i == endidx) {
								status.setLast(true);
							} else {
								status.setLast(false);
							}
							status.setBegin(0);
							status.setEnd(endidx);
							status.setCurrent(values[i]);
							status.setIndex(i);
							status.setCount(count);
							getJspContext().setAttribute(varStatus, status);
						}
						getJspBody().invoke(null);
						count++;
					}
				}
			} else if (items instanceof Map) {
				Map target = (Map) items;
				Set emtrySet = target.entrySet();
				Object[] entry = emtrySet.toArray();
				if (reverse) {
					for (int i = entry.length - 1, len = entry.length - 1; i >= 0; i--) {
						if (checkBreak) {
							Object breakValue = (Object) getJspContext().getAttribute(breakConditionName);
							if (breakValue != null && Boolean.valueOf(breakValue.toString()).booleanValue() == true) {
								break;
							}
						}
						getJspContext().setAttribute(var, (Map.Entry) entry[i]);
						if (checkStatus) {
							if (i == len) {
								status.setFirst(true);
							} else {
								status.setFirst(false);
							}
							if (i == 0) {
								status.setLast(true);
							} else {
								status.setLast(false);
							}
							status.setBegin(len);
							status.setEnd(0);
							status.setCurrent((Map.Entry) entry[i]);
							status.setIndex(i);
							status.setCount(count);
							getJspContext().setAttribute(varStatus, status);
						}
						getJspBody().invoke(null);
						count++;
					}
				} else {
					for (int i = 0, len = entry.length, endidx = entry.length - 1; i < len; i++) {
						if (checkBreak) {
							Object breakValue = (Object) getJspContext().getAttribute(breakConditionName);
							if (breakValue != null && Boolean.valueOf(breakValue.toString()).booleanValue() == true) {
								break;
							}
						}
						getJspContext().setAttribute(var, (Map.Entry) entry[i]);
						if (checkStatus) {
							if (i == 0) {
								status.setFirst(true);
							} else {
								status.setFirst(false);
							}
							if (i == endidx) {
								status.setLast(true);
							} else {
								status.setLast(false);
							}
							status.setBegin(0);
							status.setEnd(endidx);
							status.setCurrent((Map.Entry) entry[i]);
							status.setIndex(i);
							status.setCount(count);
							getJspContext().setAttribute(varStatus, status);
						}
						getJspBody().invoke(null);
						count++;
					}
				}
			} else if (items instanceof ResultSet) {
				ResultSet rs = (ResultSet) items;
				ResultSetMetaData rsmd = null;
				int columnCount = 0;
				String[] columnNames = null;
				try {
					if (reverse && rs.getFetchDirection() != ResultSet.TYPE_FORWARD_ONLY) {
						rs.afterLast();
						rsmd = getResultSetMetaData(rs);
						columnCount = rsmd.getColumnCount();
						columnNames = getColumnNames(rsmd, columnCount);
						Map<String, Object> map = new HashMap<String, Object>();
						while (rs.previous()) {
							if (checkBreak) {
								Object breakValue = (Object) getJspContext().getAttribute(breakConditionName);
								if (breakValue != null && Boolean.valueOf(breakValue.toString()).booleanValue() == true) {
									break;
								}
							}
							fillMap(rs, map, rsmd, columnCount, columnNames);
							getJspContext().setAttribute(var, map);
							if (checkStatus) {
								status.setFirst(rs.isLast());
								status.setFirst(rs.isFirst());
								status.setCurrent(map);
								status.setIndex(count - 1);
								status.setCount(count);
								getJspContext().setAttribute(varStatus, status);
							}
							getJspBody().invoke(null);
							count++;
						}
					} else {
						rs.beforeFirst();
						rsmd = getResultSetMetaData(rs);
						columnCount = rsmd.getColumnCount();
						columnNames = getColumnNames(rsmd, columnCount);
						Map<String, Object> map = new HashMap<String, Object>();
						while (rs.next()) {
							if (checkBreak) {
								Object breakValue = (Object) getJspContext().getAttribute(breakConditionName);
								if (breakValue != null && Boolean.valueOf(breakValue.toString()).booleanValue() == true) {
									break;
								}
							}
							fillMap(rs, map, rsmd, columnCount, columnNames);
							getJspContext().setAttribute(var, map);
							if (checkStatus) {
								status.setFirst(rs.isFirst());
								status.setFirst(rs.isLast());
								status.setCurrent(map);
								status.setIndex(count - 1);
								status.setCount(count);
								getJspContext().setAttribute(varStatus, status);
							}
							getJspBody().invoke(null);
							count++;
						}
					}
				} catch (SQLException ex) {
					IOException exx = new IOException(ex.getMessage());
					exx.setStackTrace(ex.getStackTrace());
					throw exx;
				} finally {
					rsmd = null;
					columnNames = null;
				}
			} else if (items.getClass().isArray()) {
				List list = Arrays.asList((Object[]) items);
				Object[] values = list.toArray();
				if (reverse) {
					for (int i = values.length - 1, len = values.length - 1; i >= 0; i--) {
						if (checkBreak) {
							Object breakValue = (Object) getJspContext().getAttribute(breakConditionName);
							if (breakValue != null && Boolean.valueOf(breakValue.toString()).booleanValue() == true) {
								break;
							}
						}
						getJspContext().setAttribute(var, values[i]);
						if (checkStatus) {
							if (i == len) {
								status.setFirst(true);
							} else {
								status.setFirst(false);
							}
							if (i == 0) {
								status.setLast(true);
							} else {
								status.setLast(false);
							}
							status.setBegin(len);
							status.setEnd(0);
							status.setCurrent(values[i]);
							status.setIndex(i);
							status.setCount(count);
							getJspContext().setAttribute(varStatus, status);
						}
						getJspBody().invoke(null);
						count++;
					}
				} else {
					for (int i = 0, len = values.length, endidx = values.length - 1; i < len; i++) {
						if (checkBreak) {
							Object breakValue = (Object) getJspContext().getAttribute(breakConditionName);
							if (breakValue != null && Boolean.valueOf(breakValue.toString()).booleanValue() == true) {
								break;
							}
						}
						getJspContext().setAttribute(var, values[i]);
						if (checkStatus) {
							if (i == 0) {
								status.setFirst(true);
							} else {
								status.setFirst(false);
							}
							if (i == endidx) {
								status.setLast(true);
							} else {
								status.setLast(false);
							}
							status.setBegin(0);
							status.setEnd(endidx);
							status.setCurrent(values[i]);
							status.setIndex(i);
							status.setCount(count);
							getJspContext().setAttribute(varStatus, status);
						}
						getJspBody().invoke(null);
						count++;
					}
				}
			}
		} else if (begin != 0 && end != 0) {
			int idxstep = status.getStep();
			if (reverse) {
				for (int i = end; i >= begin; i -= idxstep) {
					if (checkBreak) {
						Object breakValue = (Object) getJspContext().getAttribute(breakConditionName);
						if (breakValue != null && Boolean.valueOf(breakValue.toString()).booleanValue() == true) {
							break;
						}
					}
					Integer value = new Integer(i);
					getJspContext().setAttribute(var, value);
					if (checkStatus) {
						if (i == end) {
							status.setFirst(true);
						} else {
							status.setFirst(false);
						}
						if (i == begin) {
							status.setLast(true);
						} else {
							status.setLast(false);
						}
						status.setBegin(end);
						status.setEnd(begin);
						status.setCurrent(value);
						status.setIndex(i);
						status.setCount(count);
						status.setStep(idxstep);
						getJspContext().setAttribute(varStatus, status);
					}
					getJspBody().invoke(null);
					count++;
				}
			} else {
				for (int i = begin; i <= end; i += idxstep) {
					if (checkBreak) {
						Object breakValue = (Object) getJspContext().getAttribute(breakConditionName);
						if (breakValue != null && Boolean.valueOf(breakValue.toString()).booleanValue() == true) {
							break;
						}
					}
					Integer value = new Integer(i);
					getJspContext().setAttribute(var, value);
					if (checkStatus) {
						if (i == begin) {
							status.setFirst(true);
						} else {
							status.setFirst(false);
						}
						if (i == end) {
							status.setLast(true);
						} else {
							status.setLast(false);
						}
						status.setBegin(begin);
						status.setEnd(end);
						status.setCurrent(value);
						status.setIndex(i);
						status.setCount(count);
						status.setStep(idxstep);
						getJspContext().setAttribute(varStatus, status);
					}
					getJspBody().invoke(null);
					count++;
				}
			}
		}
	}

	private ResultSetMetaData getResultSetMetaData(ResultSet rs) throws SQLException {
		return rs.getMetaData();
	}

	private String[] getColumnNames(ResultSetMetaData rsmd, int columnCount) throws SQLException {
		String[] columnNames = new String[columnCount];
		for (int i = 1; i <= columnCount; i++) {
			String columnName = rsmd.getColumnLabel(i);
			if (columnName == null) {
				columnName = rsmd.getColumnName(i);
			}
			columnNames[i - 1] = columnName;
		}
		return columnNames;
	}

	private void fillMap(ResultSet rs, Map<String, Object> map, ResultSetMetaData rsmd, int columnCount, String[] columnNames) throws SQLException {
		try {
			map.clear();
			for (int i = 1; i <= columnCount; i++) {
				String columnName = columnNames[i];
				map.put(columnName, rs.getObject(columnName));
			}
		} catch (SQLException ex) {
			throw ex;
		} finally {
			rsmd = null;
		}
	}
}
