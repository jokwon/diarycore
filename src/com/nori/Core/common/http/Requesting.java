package com.nori.Core.common.http;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;

public class Requesting {
	private Requesting() {}

	public static boolean isAjax(HttpServletRequest request) {
		String accept = StringUtils.lowerCase(request.getHeader("accept"));
		String ajax = request.getHeader("X-Requested-With");

		return ( StringUtils.indexOf(accept, "json") > -1 || StringUtils.isNotEmpty(ajax));
	}
}
