package com.nori.Core.validation.constraint;


import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.nori.Core.validation.type.PhoneType;
import com.nori.Core.validation.validator.KoreanPhoneNumberValidator;

@Target(value = {ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER})
@Retention(value = RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {KoreanPhoneNumberValidator.class})
public @interface KoreanPhoneNumber {

	String message() default "{kr.co.funzin.validation.koreanphonenumber}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

	PhoneType[] type();

	@Target(value = {ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER})
	@Retention(value = RetentionPolicy.RUNTIME)
	@Documented
	public @interface List {

		KoreanPhoneNumber[] value();
	}
}
