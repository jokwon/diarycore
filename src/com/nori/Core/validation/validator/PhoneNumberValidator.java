package com.nori.Core.validation.validator;

import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.nori.Core.validation.constraint.TelNo;

public class PhoneNumberValidator implements ConstraintValidator<TelNo, String> {

	//private static final Pattern PATTERN_PHONE = Pattern.compile("^(\\+)?(\\d)+(([\\s\\-])*(\\d)+)+$");
	private static final Pattern PATTERN_PHONE = Pattern.compile("^(\\d\\d(\\d)?(\\d)?)?([-])?(\\d\\d\\d(\\d)?)+([-])?(\\d\\d\\d\\d)+$");

	@Override
	public void initialize(TelNo annotation) {
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		if (value == null || "".equals(value)) {
			return true;
		}

		if (PATTERN_PHONE.matcher(value).matches()) {
			return true;
		}

		return false;
	}
}
