package com.nori.Core.validation.type;

public enum PhoneType {

	LANDLINE,
	MOBILE,
	INTERNET,
	PHONE
}
