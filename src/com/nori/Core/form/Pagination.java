package com.nori.Core.form;

import org.springframework.format.annotation.NumberFormat;

public class Pagination {
	@NumberFormat
	private int pageSize;
	@NumberFormat
	private long pageNo;
	@NumberFormat
	private long totalCount;
	@NumberFormat
	private long pagePrev;
	@NumberFormat
	private long pageNext;
	@NumberFormat
	private long pageTotal;
	@NumberFormat
	private long paginationPrev;
	@NumberFormat
	private long paginationNext;
	@NumberFormat
	private long countFromMax;
	@NumberFormat
	private long countFromZero;
	@NumberFormat
	private long fetchSize;
	@NumberFormat
	private int paginationSize;
	private String sort;
	private String order;
	private boolean fetchAll;

	public Pagination() {
		this.pageNo = 1;
		this.pageSize = 5;
		this.paginationSize = 10;
	}

	public void computePagination() {
		long tCount = totalCount;
		long pNo = pageNo;
		int pSize = pageSize;
		long tPage = 0;
		long pPrev = 0;
		long pNext = 0;
		long cLocation = 0;
		long sPage = 0;
		long ePage = 0;

		if (pNo <= 0) {
			pNo = 1;
		}
		if (pSize < 0) {
			pageNo = 1;
			pagePrev = 1;
			pageNext = 1;
			pageTotal = 1;
			paginationPrev = 1;
			paginationNext = 1;
			totalCount = tCount;
			countFromMax = tCount;
			countFromZero = 0;
			fetchSize = tCount;
		} else {
			if (pSize == 0) {
				pSize = 10;
			}
			tPage = tCount / pSize;

			if ((tCount % pSize) > 0) {
				tPage++;
			}
			if (pNo > tPage) {
				pNo = tPage;
			}
			pPrev = pNo - 1;

			pNext = pNo + 1;

			if (pPrev < 1) {
				pPrev = 1;
			}
			if (pNext > tPage) {
				pNext = tPage;
			}
			sPage = ((pNo - 1) / paginationSize) * paginationSize;

			ePage = sPage + paginationSize - 1;

			if (ePage > (tPage - 1)) {
				ePage = tPage - 1;
			}
			cLocation = pSize * (pNo - 1);
			if (cLocation < 0) {
				cLocation = 0;
			}
			long iCountMax = tCount - cLocation;

			pageSize = pSize;
			pageNo = pNo;
			pagePrev = pPrev;
			pageNext = pNext;
			pageTotal = tPage;
			paginationPrev = sPage + 1;
			paginationNext = ePage + 1;
			totalCount = tCount;
			countFromMax = iCountMax;
			countFromZero = cLocation;

			fetchSize = pageSize;
			if (pageNo == pageTotal) {
				fetchSize = countFromMax;
			}
		}
	}

	public long getCountFromMax() {
		return countFromMax;
	}

	public long getCountFromZero() {
		return countFromZero;
	}

	public long getFetchSize() {
		return fetchSize;
	}

	public long getPageNext() {
		return pageNext;
	}

	public long getPageNo() {
		return pageNo;
	}

	public long getPagePrev() {
		return pagePrev;
	}

	public int getPageSize() {
		return pageSize;
	}

	public long getPageTotal() {
		return pageTotal;
	}

	public long getPaginationNext() {
		return paginationNext;
	}

	public long getPaginationPrev() {
		return paginationPrev;
	}

	public int getPaginationSize() {
		return paginationSize;
	}

	public long getTotalCount() {
		return totalCount;
	}

	public void setPage(long page) {
		this.pageNo = page;
	}

	public void setPageNo(long pageNo) {
		this.pageNo = pageNo;
	}

	public void setRows(int rows) {
		this.pageSize = rows;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setLimit(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public boolean isFetchAll() {
		return fetchAll;
	}

	public void setFetchAll(boolean fetchAll) {
		this.fetchAll = fetchAll;
	}
}
