package com.nori.Core.view;

import java.io.OutputStream;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.view.AbstractView;

import com.nori.Core.utility.ArrayUtil;

public class ImageView extends AbstractView {

	private static Logger logger = Logger.getLogger(ImageView.class);
	
	private String mime = "image/jpeg";

	public String getMime() {
		return mime;
	}

	public void setMime(String mime) {
		this.mime = mime;
	}

	@Override
	protected void renderMergedOutputModel(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		byte[] image = (byte[]) model.get("image");
		if (!ArrayUtil.isNullOrEmpty(image)) {
			OutputStream fout = null;
			try {
				response.reset();
				response.setContentType(mime);
				response.setHeader("Content-Length", "" + image.length);
				fout = response.getOutputStream();
				fout.write(image, 0, image.length);
				fout.flush();
			} catch (Exception e) {
				logger.fatal(e, e);
				throw e;
			} finally {
				try {
					if (fout != null) {
						fout.close();
					}
				} catch (Exception ex) {
				} finally {
					fout = null;
				}
				image = null;
			}
		}
	}
}
