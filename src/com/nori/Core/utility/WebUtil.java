package com.nori.Core.utility;

import java.util.Calendar;
import java.util.Enumeration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class WebUtil {
	
	protected static Log log = LogFactory.getLog(WebUtil.class);
	
	public static Pattern PATTERN_PHONE = Pattern.compile("^(01[016789]{1}|02|0[3-9]{1}[0-9]{1})-?[0-9]{3,4}-?[0-9]{4}$");
	public static Pattern PATTERN_TEL = Pattern.compile("^(01\\d{1}|02|0505|0502|0506|0\\d{1,2})-?(\\d{3,4})-?(\\d{4})");

	private WebUtil() {
	}
	
	/*public static String printEstimatedTime(Number estimatedTime) {
		if (estimatedTime != null) {
			double value = estimatedTime.doubleValue();
			String point = Double.toString(value);
			if (point.indexOf(".") != -1) {
				point = point.substring(point.indexOf("."));
			} else {
				point = ".0";
			}
			long hour = (long)Math.floor(value / 3600);
			long minute = (long)Math.floor((value - (hour * 3600)) / 60);
			long second = (long)(value - (hour * 3600) - (minute * 60));
			return StringUtil.paddingLeft("" + hour, "0", 2) + " :" + StringUtil.paddingLeft("" + minute, "0", 2, 2) + ":" + StringUtil.paddingLeft("" + second, "0", 2, 2) + point;
		}
		return "";
	}*/
	
	public static String printEstimatedTime(Number estimatedTime) {
		if (estimatedTime != null) {
			double value = estimatedTime.doubleValue() / 1000d;
			if (value > 3600d) {
				long hour = (long)Math.floor(value / 3600);
				long minute = (long)Math.floor((value - (hour * 3600)) / 60);
				double second = value - (hour * 3600) - (minute * 60);
				return hour + " 시간" + (minute != 0 ? " " + minute + " 분" : "") + (second != 0d ? " " + NumberUtil.toFixed(second, 1) + " 초" : "");
			} else if (value > 60d) {
				long minute = (long)Math.floor(value / 60);
				double second = value - (minute * 60);
				return minute + " 분" + (second != 0 ? " " + NumberUtil.toFixed(second, 1) + " 초" : "");
			} else {
				return NumberUtil.toFixed(value, 1) + "  초";
			}
		}
		return "";
	}
	
	public static String printEstimatedTimeInMinutes(Number estimatedTime) {
		if (estimatedTime != null) {
			double value = estimatedTime.doubleValue() / 1000d;
			if (value > 3600d) {
				long hour = (long)Math.floor(value / 3600);
				long minute = (long)Math.floor((value - (hour * 3600)) / 60);
				double second = value - (hour * 3600) - (minute * 60);
				return hour + " 시간" + (minute != 0 ? " " + minute + " 분" : "") + (second != 0d ? " " + NumberUtil.toFixed(second, 1) + " 초" : "");
			} else {
				long minute = (long)Math.floor(value / 60);
				double second = value - (minute * 60);
				return minute + " 분" + (second != 0 ? " " + NumberUtil.toFixed(second, 1) + " 초" : "");
			}
		}
		return "";
	}
	
	
	public static String getTodayDate(int len) {
		
		Calendar oCalendar = Calendar.getInstance( );  
		
		String year  = intToString(oCalendar.get(Calendar.YEAR));
		String month = intToString(oCalendar.get(Calendar.MONTH) + 1);
		String day   = intToString(oCalendar.get(Calendar.DAY_OF_MONTH));
		String hour  = intToString(oCalendar.get(Calendar.HOUR_OF_DAY));
		String min   = intToString(oCalendar.get(Calendar.MINUTE));
		String sec   = intToString(oCalendar.get(Calendar.SECOND));
		 
		if(month.length() == 1) month = "0" + month;
		if(day.length() == 1) day = "0" + day;
		if(hour.length() == 1) hour = "0" + hour;
		if(min.length() == 1) min = "0" + min;
		if(sec.length() == 1) sec = "0" + sec;      
		          
		String tmp = year+month+day+hour+min+sec;	    
		
		return tmp.substring(0,len);
	}
	
	private static String intToString(int i){
        return Integer.toString(i);
    }
	
	public static String getRequestAttributesString(HttpServletRequest request){
		String attrString = "";
		try {
			Enumeration attrNames = request.getAttributeNames();
	        while(attrNames.hasMoreElements()){
	            String name = (String)attrNames.nextElement();
	            Object value = request.getAttribute(name);
	            attrString += name + " : " + value + "\n";
	        }
		} catch (Exception e) {
			e.printStackTrace();
		}
		return attrString;
	}
	
	public static String getSessionAttributesString(HttpServletRequest request){
		String attrString = "";
		try {
			HttpSession session = request.getSession(false);
			if(session == null){
				return attrString;
			}
			Enumeration attrNames = session.getAttributeNames();
	        while(attrNames.hasMoreElements()){
	            String name = (String)attrNames.nextElement();
	            Object value = session.getAttribute(name);
	            attrString += name + " : " + value + "\n";
	        }
		} catch (Exception e) {
			e.printStackTrace();
		}
		return attrString;
	}
	
	public static String getRequestHeadersString(HttpServletRequest request){
		String header = "";
		try {
			Enumeration headerNames = request.getHeaderNames();
	        while(headerNames.hasMoreElements()){
	            String name = (String)headerNames.nextElement();
	            String value = request.getHeader(name);
	            header += name + " : " + value + "\n";
	        }
		} catch (Exception e) {
			e.printStackTrace();
		}
		return header;
	}
	
	public static String getRequestParamsString(HttpServletRequest request){
		String params = "";
		try {
			Enumeration<String> parameterNames = request.getParameterNames();
			while (parameterNames.hasMoreElements()) {
				String paramName = parameterNames.nextElement();
				String[] paramValues = request.getParameterValues(paramName);
				for (int i = 0; i < paramValues.length; i++) {
					params += paramName+"="+paramValues[i]+"\n";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return params;
	}
	
	public static void printDetailError(HttpServletRequest request, Throwable t){
		String requestUri = request.getRequestURI();
//		//이 에러는 언제든 발생할수있고 방지할수 없고 빈번하다 그래서 예외처리
//		if( "org.apache.catalina.connector.ClientAbortException".equals( t.getClass().getName() )//jenkins빌드하다 메이븐쪽에서 의존성 에러나길래 스트링비교로 처리 (instanceof 쓰고싶으면 해결하고 쓰세요..)
//				|| ( "java.lang.IllegalStateException".equals( t.getClass().getName() ) 
//						&& (requestUri.startsWith("/css/") 
//							|| requestUri.startsWith("/html/") 
//							|| requestUri.startsWith("/images/") 
//							|| requestUri.startsWith("/js/") )
//					)
//			){
//			log.debug( ">> excelude error log - "+t );
//			return;
//		}
		
		log.error("\n----request url - "+requestUri+
				"\n----request attrs info----\n"+getRequestAttributesString(request)+
				"\n----session attrs info----\n"+getSessionAttributesString(request)+
				"\n----header info----\n"+getRequestHeadersString(request)+
				"\n----params info----\n"+getRequestParamsString(request)
				,t);
	}
	
	/**
	 * 전화번호를 나눠서 문자배열로 반환.
	 * @param noStr
	 * @return
	 */
	public static String[] getSplitTelNo(String noStr){
		if(noStr == null) return new String[]{"","",""};
		Matcher matcher = PATTERN_TEL.matcher(noStr);
		if(matcher.matches()){
			return new String[]{matcher.group(1), matcher.group(2), matcher.group(3)};
		} else {
			return new String[]{"","",""};
		}
	}

	/**
	 * 전화번호에 하이픈을 넣어준다.
	 * @param telNo
	 * @return
	 */
	public static String setHypen(String telNo){
		String[] telArr = getSplitTelNo( telNo );
		return telArr[0]+"-"+telArr[1]+"-"+telArr[2];
	}
	
	public static String getClientIp(HttpServletRequest request){
		String ip = request.getHeader("X-Forwarded-For");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) { 
		    ip = request.getHeader("Proxy-Client-IP"); 
		} 
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) { 
		    ip = request.getHeader("WL-Proxy-Client-IP"); 
		} 
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) { 
		    ip = request.getHeader("HTTP_CLIENT_IP"); 
		} 
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) { 
		    ip = request.getHeader("HTTP_X_FORWARDED_FOR"); 
		} 
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) { 
		    ip = request.getRemoteAddr(); 
		}
        return ip;
	}
}
