package com.nori.Core.utility;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

public class CompressUtil {
	
	private CompressUtil() {
		
	}
	
	public static byte[] compress(byte[] dataByte) {

		Deflater deflater = new Deflater();

		deflater.setLevel(Deflater.BEST_COMPRESSION);
		deflater.setInput(dataByte);
		deflater.finish();

		ByteArrayOutputStream bao = null;
		try {
			bao = new ByteArrayOutputStream(dataByte.length);
			byte[] buf = new byte[8192];
			while (!deflater.finished()) {
				int compByte = deflater.deflate(buf);
				bao.write(buf, 0, compByte);
			}

			return bao.toByteArray();
		} catch (Exception ex) {

		} finally {
			if (bao != null) {
				try {
					bao.close();
				} catch (IOException ex) {
				}
			}
			bao = null;
			deflater.end();
		}

		return null;
	}

	public static byte[] decompress(byte[] data) {
		Inflater inflater = new Inflater();
		inflater.setInput(data);

		ByteArrayOutputStream bao = null;
		try {
			bao = new ByteArrayOutputStream();
			byte[] buf = new byte[8192];
			while (!inflater.finished()) {
				int compByte = inflater.inflate(buf);
				bao.write(buf, 0, compByte);
			}

			return bao.toByteArray();
		} catch (Exception ex) {

		} finally {
			if (bao != null) {
				try {
					bao.close();
				} catch (IOException ex) {
				}
			}
			bao = null;
			inflater.end();
		}

		return null;
	}
}
