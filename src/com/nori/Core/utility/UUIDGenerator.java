package com.nori.Core.utility;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

public class UUIDGenerator {

	private static String baseUUID = null;
	private static long incrementingValue = 0;
	private static SecureRandom sRand = null;

	public static String getUUID() {
		if (baseUUID == null) {
			getInitialUUID();
		}
		long i = ++incrementingValue;
		if (i >= Long.MAX_VALUE || i < 0) {
			incrementingValue = 0;
			i = 0;
		}
		String current = Long.toString(System.currentTimeMillis());
		current = StringUtil.paddingLeft(current, "0", 14).substring(0, 14);
		byte[] salt = new byte[5];
		sRand.nextBytes(salt);
		return baseUUID + "-" + current + "-" + HexUtil.toHex(salt);
	}

	protected static synchronized void getInitialUUID() {
		if (baseUUID != null) {
			return;
		}
		if (sRand == null) {
			try {
				sRand = SecureRandom.getInstance("SHA1PRNG");
			} catch (Exception ex) {
				sRand = (SecureRandom) new Random();
			}
		}
		long rand = sRand.nextLong();
		String sid;
		try {
			sid = InetAddress.getLocalHost().toString();
		} catch (UnknownHostException e) {
			sid = Thread.currentThread().getName();
		}
		StringBuffer sb = new StringBuffer();
		sb.append(sid);
		sb.append(":");
		sb.append(Long.toString(rand));
		MessageDigest hash = null;
		try {
			hash = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
		}
		hash.update(sb.toString().getBytes());
		byte[] array = hash.digest();
		StringBuffer sb2 = new StringBuffer();
		for (int j = 0; j < array.length; ++j) {
			int b = array[j] & 0xFF;
			sb2.append(Integer.toHexString(b));
		}
		baseUUID = sb2.toString().substring(0, 8).toLowerCase();
	}
}
