package com.nori.Core.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Row.MissingCellPolicy;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class PoiExcelUtil {


	
	public static ArrayList<HashMap<String, Object>> insertRow(File file, int bodyStartIndex) throws IOException {

		ArrayList<HashMap<String, Object>> mapList = new ArrayList<>();
		FileInputStream fis = new FileInputStream(file);

		/* Load workbook */
		@SuppressWarnings("resource")
		XSSFWorkbook workbook = new XSSFWorkbook(fis);
		/* Load worksheet */
		XSSFSheet sheet = workbook.getSheetAt(0);
		// we loop through and insert data

		DataFormatter formatter = new DataFormatter();
		Sheet sheet1 = workbook.getSheetAt(0);

		int rowIndex = 0;

		ArrayList<String> keys = new ArrayList<>();
		for (Row row : sheet1) {

			
			HashMap<String, Object> map = new HashMap<>();
			String preHeaderName = "";

			for (int cellIndex = 0; cellIndex < row.getLastCellNum(); cellIndex++) {
				Cell cell = row.getCell(cellIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK);
				CellReference cellRef = new CellReference(row.getRowNum(), cell.getColumnIndex());
//				System.out.print(cellRef.formatAsString());
//				System.out.print(" - ");
//				
//				String text = formatter.formatCellValue(cell);
//				

				// header key map 생성

				if (rowIndex < bodyStartIndex) {
					
					if (rowIndex == 0) keys.add(cell.getStringCellValue());
					else {
						
						if (!"".equals( formatter.formatCellValue(cell)) && !"".equals(keys.get(cellIndex)) ) preHeaderName = keys.get(cellIndex);
						if ( keys.get(cellIndex).equals( formatter.formatCellValue(cell)) ) preHeaderName = "";
						
						if("".equals( formatter.formatCellValue(cell)))
							keys.set(cellIndex, keys.get(cellIndex));
						else
							keys.set(cellIndex, preHeaderName +"_"+ formatter.formatCellValue(cell));
					}
					
					
					
				} else {
					preHeaderName = "";
				}



				// body 생성
				if (cell.getCellTypeEnum().equals(CellType.STRING)) {
					map.put(keys.get(cellIndex), cell.getStringCellValue());
				} else if (cell.getCellTypeEnum().equals(CellType.NUMERIC)) {
					if (DateUtil.isCellDateFormatted(cell)) {
						map.put(keys.get(cellIndex),
								com.nori.Core.utility.DateUtil.formatDate(cell.getDateCellValue(), "YYYY-MM-dd"));
					} else {
						map.put(keys.get(cellIndex), String.valueOf(Math.round(cell.getNumericCellValue())));
					}
				} else if (cell.getCellTypeEnum().equals(CellType.BOOLEAN)) {
					map.put(keys.get(cellIndex), cell.getBooleanCellValue());
				} else if (cell.getCellTypeEnum().equals(CellType.FORMULA)) {
					map.put(keys.get(cellIndex), cell.getCellFormula());
				} else if (cell.getCellTypeEnum().equals(CellType.BLANK)) {
					map.put(keys.get(cellIndex), "");
				} else {
					
				}
			}

			if (rowIndex >= bodyStartIndex) {
				mapList.add(map);
			}
			rowIndex++;
		}
		
		/* Close input stream */
		fis.close();

		return mapList;
	}
	

}
