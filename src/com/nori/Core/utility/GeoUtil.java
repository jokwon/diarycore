package com.nori.Core.utility;

import com.google.code.geocoder.GeocoderRequestBuilder;
import com.google.code.geocoder.model.GeocodeResponse;
import com.google.code.geocoder.model.GeocoderRequest;
import com.google.code.geocoder.model.GeocoderResult;
import com.google.code.geocoder.model.GeocoderStatus;
import com.google.code.geocoder.model.LatLng;

public class GeoUtil {
	
	private GeoUtil() {
	}
	
	/***
	 * 두지점 ( 위도, 경도 ) 간 거리계산 
	 * @param latitude1
	 * @param longitude1
	 * @param latitude2
	 * @param longitude2
	 * @return 값 단위 km
	 */
    public static Double getDistanceBetweenTwoPoints(Double[] p1, Double[] p2) {
		
		Double latitude1 = p1[0];
		Double longitude1 = p1[1];

		Double latitude2 = p2[0];
		Double longitude2 = p2[1];
	    
		final int RADIUS_EARTH = 6371;
	
	    double dLat = getRad(latitude2 - latitude1);
	    double dLong = getRad(longitude2 - longitude1);
	
	    double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(getRad(latitude1)) * Math.cos(getRad(latitude2)) * Math.sin(dLong / 2) * Math.sin(dLong / 2);
	    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	    return (RADIUS_EARTH * c);
    }
	
	private static Double getRad(Double x) {
	    return x * Math.PI / 180;
    }


	public static Double[] geoCoding(String location) {

		if (location == null)  

		return null;
				       
		NewGeocoder geocoder = new NewGeocoder(true);
		
		// setAddress : 변환하려는 주소 (경기도 성남시 분당구 등)
		// setLanguate : 인코딩 설정

		GeocoderRequest geocoderRequest = new GeocoderRequestBuilder().setAddress(location).setLanguage("ko").getGeocoderRequest();
	
		GeocodeResponse geocoderResponse;

		geocoderResponse = geocoder.geocode(geocoderRequest);
		
		if (geocoderResponse.getStatus() == GeocoderStatus.OK & !geocoderResponse.getResults().isEmpty()) {
			GeocoderResult geocoderResult=geocoderResponse.getResults().iterator().next();
			LatLng latitudeLongitude = geocoderResult.getGeometry().getLocation();
	
			Double[] coords = new Double[2];
			coords[0] = latitudeLongitude.getLat().doubleValue();
			coords[1] = latitudeLongitude.getLng().doubleValue();
			
			return coords;
		}else {
			
		}

		return null;

	}
	
	
}
