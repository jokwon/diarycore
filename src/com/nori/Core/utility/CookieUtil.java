/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nori.Core.utility;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ChangHoon
 */
public class CookieUtil {

	private CookieUtil() {
	}

	public static List<String> getCookieNames(HttpServletRequest request) {
		List<String> list = new ArrayList<String>();
		Cookie cookie[] = request.getCookies();

		for (int i = 0; cookie != null && i < cookie.length; i++) {
			list.add(cookie[i].getName());
		}

		return list;
	}

	public static void setCookie(HttpServletRequest request, HttpServletResponse response, String name, String value,
			  String domain, String path, int expiry, String comment, boolean secure, int version) {
		Cookie cookie[] = request.getCookies();
		Cookie target_cookie = null;
		for (int i = 0; cookie != null && i < cookie.length; i++) {
			if (cookie[i].getName().equals(name.trim())) {
				target_cookie = cookie[i];
				target_cookie.setValue(value.trim());
				break;
			}
		}

		if (target_cookie == null) {
			target_cookie = new Cookie(name.trim(), value.trim());
		}

		if (domain != null && !"".equals(domain)) {
			target_cookie.setDomain(domain);
		}
		if (path != null && !"".equals(path)) {
			target_cookie.setPath(path);
		}
		target_cookie.setComment(comment);
		target_cookie.setMaxAge(expiry);
		target_cookie.setSecure(secure);
		if (version > 0) {
			target_cookie.setVersion(version);
		}
		response.addCookie(target_cookie);
	}

	public static void setCookie(HttpServletRequest request, HttpServletResponse response, String name, String value,
			  String domain, String path, int expiry, String comment, boolean secure) {
		setCookie(request, response, name, value, domain, path, expiry, comment, secure, 1);
	}

	public static void setCookie(HttpServletRequest request, HttpServletResponse response, String name, String value,
			  String domain, String path, int expiry, String comment) {
		setCookie(request, response, name, value, domain, path, expiry, comment, false, 1);
	}

	public static void setCookie(HttpServletRequest request, HttpServletResponse response, String name, String value,
			  String domain, String path, int expiry) {
		setCookie(request, response, name, value, domain, path, expiry, "", false, 1);
	}

	public static void setCookie(HttpServletRequest request, HttpServletResponse response, String name, String value,
			  String domain, String path) {
		setCookie(request, response, name, value, domain, path, -1, "", false, 1);
	}

	public static void setCookie(HttpServletRequest request, HttpServletResponse response, String name, String value,
			  String domain, int expiry) {
		setCookie(request, response, name, value, domain, "/", expiry, "", false, 1);
	}

	public static void setCookie(HttpServletRequest request, HttpServletResponse response, String name, String value,
			  String domain) {
		setCookie(request, response, name, value, domain, "/", -1, "", false, 1);
	}

	public static void setCookie(HttpServletRequest request, HttpServletResponse response, String name, String value) {
		setCookie(request, response, name, value, request.getServerName(), "/", -1, "", false, 1);
	}

	public static Cookie getCookie(HttpServletRequest request, String name) {
		Cookie cookie[] = request.getCookies();
		for (int i = 0; cookie != null && i < cookie.length; i++) {
			if (cookie[i].getName().equals(name)) {
				return cookie[i];
			}
		}
		return null;
	}
	
	public static Cookie getCookie(HttpServletRequest request, String name, boolean secure) {
		Cookie cookie[] = request.getCookies();
		for (int i = 0; cookie != null && i < cookie.length; i++) {
			if (cookie[i].getName().equals(name) && cookie[i].getSecure() == secure) {
				return cookie[i];
			}
		}
		return null;
	}

	public static void removeCookie(HttpServletRequest request, HttpServletResponse response, String name) {
		Cookie cookie = new Cookie(name, "");
		cookie.setMaxAge(0);
		response.addCookie(cookie);
	}

	public static void removeCookie(HttpServletRequest request, HttpServletResponse response, String name, String domain) {
		Cookie cookie = new Cookie(name, "");
		cookie.setMaxAge(0);
		cookie.setDomain(domain);
		response.addCookie(cookie);
	}

	public static void removeCookie(HttpServletRequest request, HttpServletResponse response, String name, String domain,
			  String path) {
		Cookie cookie = new Cookie(name, "");
		cookie.setMaxAge(0);
		cookie.setDomain(domain);
		cookie.setPath(path);
		response.addCookie(cookie);
	}
}
