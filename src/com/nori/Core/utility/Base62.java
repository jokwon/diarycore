package com.nori.Core.utility;

import java.math.BigInteger;

public class Base62 {

	public static final BigInteger BASE = BigInteger.valueOf(62);
	public static final String DIGITS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	public static final String REGEXP = "^[0-9A-Za-z]+$";

	private Base62() {
	}

	/**
	 * Encodes a number using Base62 encoding.
	 *
	 * @param number
	 *            a positive integer
	 * @return a Base62 string
	 * @throws IllegalArgumentException
	 *             if <code>number</code> is a negative integer
	 */
	public static String encode(BigInteger number) {
		if (number.compareTo(BigInteger.ZERO) == -1) { // number < 0
			throw new IllegalArgumentException("number must not be negative");
		}
		StringBuilder result = new StringBuilder();
		while (number.compareTo(BigInteger.ZERO) == 1) { // number > 0
			BigInteger[] divmod = number.divideAndRemainder(BASE);
			number = divmod[0];
			int digit = divmod[1].intValue();
			result.insert(0, DIGITS.charAt(digit));
		}
		return (result.length() == 0) ? DIGITS.substring(0, 1) : result.toString();
	}

	public static String encode(long number) {
		return encode(BigInteger.valueOf(number));
	}

	/**
	 * Decodes a string using Base62 encoding.
	 *
	 * @param string
	 *            a Base62 string
	 * @return a positive integer
	 * @throws IllegalArgumentException
	 *             if <code>string</code> is empty
	 */
	public static BigInteger decode(final String string) {
		if (string.length() == 0) {
			throw new IllegalArgumentException("string must not be empty");
		}
		BigInteger result = BigInteger.ZERO;
		int digits = string.length();
		for (int index = 0; index < digits; index++) {
			int digit = DIGITS.indexOf(string.charAt(digits - index - 1));
			result = result.add(BigInteger.valueOf(digit).multiply(BASE.pow(index)));
		}
		return result;
	}

	public static String encodeString(final String plain) {
		String base64 = Base64.Encode(plain, false);
		return base64ToBase62(base64);
	}

	public static String decodeString(final String base62) {
		String base64 = base62ToBase64(base62);
		return Base64.DecodeAsString(base64);
	}

	public static String encodeFromBytes(final byte[] data) {
		String base64 = Base64.Encode(data);
		return base64ToBase62(base64);
	}

	public static byte[] decodeToBytes(final String base62) {
		String base64 = base62ToBase64(base62);
		return Base64.Decode(base64);
	}

	protected static String base64ToBase62(String base64) {
		StringBuilder buf = new StringBuilder(base64.length() * 2);

		for (int i = 0; i < base64.length(); i++) {
			char ch = base64.charAt(i);
			switch (ch) {
			case 'i':
				buf.append("ii");
				break;

			case '+':
				buf.append("ip");
				break;

			case '/':
				buf.append("is");
				break;

			case '=':
				buf.append("ie");
				break;

			case '\n':
			case '\r':
				// Strip out
				break;

			default:
				buf.append(ch);
			}
		}

		return buf.toString();
	}

	protected static String base62ToBase64(String base62) {
		StringBuilder buf = new StringBuilder(base62.length());

		int i = 0;
		while (i < base62.length()) {
			char ch = base62.charAt(i);

			if (ch == 'i') {
				i++;
				char code = base62.charAt(i);
				switch (code) {
				case 'i':
					buf.append('i');
					break;

				case 'p':
					buf.append('+');
					break;

				case 's':
					buf.append('/');
					break;

				case 'e':
					buf.append('=');
					break;

				default:
					throw new IllegalStateException("Illegal code in base62 encoding");
				}
			} else {
				buf.append(ch);
			}

			i++;
		}

		return buf.toString();
	}
}
