package com.nori.Core.utility;

import java.awt.Container;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;

//import com.sun.image.codec.jpeg.JPEGCodec;
//import com.sun.image.codec.jpeg.JPEGEncodeParam;
//import com.sun.image.codec.jpeg.JPEGImageEncoder;

public class ImageUtil {

	private static Logger logger = Logger.getLogger(ImageUtil.class);
	private Image image;
	private MediaTracker mediaTracker;
	private int imageWidth;
	private int imageHeight;

	private ImageUtil(Image image) {
		this.image = image;
	}

	private ImageUtil() {
		this.image = null;
	}

	private ImageUtil(InputStream in) {
		try {
			this.image = ImageIO.read(new BufferedInputStream(in));
		} catch (IOException ex) {
			logger.fatal(ex, ex);
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException ex) {
				}
			}
			in = null;
		}
	}

	private ImageUtil(byte[] data) {
		this.image = Toolkit.getDefaultToolkit().createImage(data);
		this.mediaTracker = new MediaTracker(new Container());
		this.mediaTracker.addImage(this.image, 0);
		try {
			this.mediaTracker.waitForID(0);
		} catch (InterruptedException e) {
		}
	}

	private ImageUtil(String path) {
		this.image = Toolkit.getDefaultToolkit().createImage(path);
		this.mediaTracker = new MediaTracker(new Container());
		this.mediaTracker.addImage(this.image, 0);
		try {
			this.mediaTracker.waitForID(0);
		} catch (InterruptedException e) {
		}
	}

	public void dispose() {
		if (this.mediaTracker != null) {
			this.mediaTracker.removeImage(this.image);
		}
		this.image = null;
		this.mediaTracker = null;
	}

	@SuppressWarnings("unused")
	private static Image readByToolkit(InputStream in) throws IOException {
		byte[] theBytes = FileUtil.streamToBytes(in);
		return Toolkit.getDefaultToolkit().createImage(theBytes);
	}

	@SuppressWarnings("unused")
	private static Image readByToolkit(byte[] src) throws IOException {
		return Toolkit.getDefaultToolkit().createImage(src);
	}

	@SuppressWarnings("unused")
	private static Image readByToolkit(String src) throws IOException {
		return Toolkit.getDefaultToolkit().createImage(src);
	}

	public static ImageUtil read(String path) {
		return new ImageUtil(path);
	}

	public static ImageUtil read(File file) {
		return new ImageUtil(file.getAbsolutePath());
	}

	public static ImageUtil read(byte[] src) {
		return new ImageUtil(src);
	}

	public static ImageUtil read(InputStream in) {
		try {
			byte[] data = FileUtil.streamToBytes(in);
			return new ImageUtil(data);
		} catch (IOException e) {
			return new ImageUtil();
		}
	}

	public int getWidth() {
		if (this.image != null && this.imageWidth == 0) {
			this.imageWidth = this.image.getWidth(null);
		}
		return this.imageWidth;
	}

	public int getHeight() {
		if (this.image != null && imageHeight == 0) {
			this.imageHeight = image.getHeight(null);
		}
		return this.imageHeight;
	}

	public void thumbnail(String thumbnailFile, int thumbWidth) {
		thumbnail(new File(thumbnailFile), thumbWidth);
	}

	public void thumbnail(File thumbnailFile, int thumbWidth) {
		FileOutputStream fout = null;
		Thumbnail thumb = null;
		try {
			thumb = getThumbnailByWidth(thumbWidth);
			fout = new FileOutputStream(thumbnailFile);
			fout.write(thumb.getThumbnail());
			fout.flush();
		} catch (Exception ex) {
			logger.fatal(ex, ex);
		} finally {
			if (fout != null) {
				try {
					fout.close();
				} catch (IOException e) {
				}
			}
			fout = null;
			thumb = null;
		}
	}

	public void thumbnailByHeight(String thumbnailFile, int thumbHeight) {
		thumbnailByHeight(new File(thumbnailFile), thumbHeight);
	}

	public void thumbnailByHeight(File thumbnailFile, int thumbHeight) {
		FileOutputStream fout = null;
		Thumbnail thumb = null;
		try {
			thumb = getThumbnailByHeight(thumbHeight);
			fout = new FileOutputStream(thumbnailFile);
			fout.write(thumb.getThumbnail());
			fout.flush();
		} catch (Exception ex) {
			logger.fatal(ex, ex);
		} finally {
			if (fout != null) {
				try {
					fout.close();
				} catch (IOException e) {
				}
			}
			fout = null;
			thumb = null;
		}
	}

	public Thumbnail getThumbnailByWidth(int width) {
		double scale = (double) width / (double) getWidth();
		width = (int) (scale * getWidth());
		int height = (int) (scale * getHeight());
		return getThumbnail(width, height);
	}

	/*
	public Thumbnail getThumbnail(int width, int height) {
		ByteArrayOutputStream baos = null;
		BufferedImage thumbImage = null;
		BufferedOutputStream out = null;
		Graphics2D graphics2D = null;
		try {
			baos = new ByteArrayOutputStream();
			out = new BufferedOutputStream(baos);
			thumbImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
			graphics2D = thumbImage.createGraphics();
			graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
			//graphics2D.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_SPEED);
			//graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
			//graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
			//graphics2D.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_DISABLE);
			//graphics2D.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_SPEED);
			graphics2D.drawImage(image, 0, 0, width, height, null);
			//ImageIO.write(thumbImage, "JPG", out);
			JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
			JPEGEncodeParam param = encoder.getDefaultJPEGEncodeParam(thumbImage);
			param.setQuality((float) 100 / 100.0f, false);
			encoder.setJPEGEncodeParam(param);
			encoder.encode(thumbImage);
			out.flush();
			return new Thumbnail(baos.toByteArray(), width, height, "image/jpeg");
		} catch (Exception ex) {
			logger.fatal(ex, ex);
			return new Thumbnail(new byte[0], 0, 0, "image/jpeg");
		} finally {
			if (baos != null) {
				try {
					baos.close();
				} catch (IOException e) {
				}
			}
			baos = null;
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
				}
			}
			out = null;
			if (graphics2D != null) {
				graphics2D.dispose();
			}
			graphics2D = null;
			if (thumbImage != null) {
				thumbImage = null;
			}
		}
	}
	*/

	public Thumbnail getThumbnail(int width, int height) {
		ByteArrayOutputStream baos = null;
		BufferedImage thumbImage = null;
		Graphics2D graphics2D = null;
		try {
			baos = new ByteArrayOutputStream();
			thumbImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
			graphics2D = thumbImage.createGraphics();
			graphics2D.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_SPEED);
			graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
			//graphics2D.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
			graphics2D.drawImage(image, 0, 0, width, height, null);
			ImageIO.write(thumbImage, "JPG", baos);
			return new Thumbnail(baos.toByteArray(), width, height, "image/jpeg");
		} catch (Exception ex) {
			logger.fatal(ex, ex);
			return new Thumbnail(new byte[0], 0, 0, "image/jpeg");
		} finally {
			if (baos != null) {
				try {
					baos.close();
				} catch (IOException e) {
				}
			}
			baos = null;
			if (graphics2D != null) {
				graphics2D.dispose();
			}
			graphics2D = null;
			if (thumbImage != null) {
				thumbImage = null;
			}
		}
	}

	public Thumbnail getThumbnailByHeight(int height) {
		double scale = (double) height / (double) getHeight();
		int width = (int) (scale * getWidth());
		height = (int) (scale * getHeight());
		return getThumbnail(width, height);
	}

	public static void saveThumbnail(String filePath, Thumbnail thumbnail) {
		try {
			FileUtil.saveBytesToFile(filePath, thumbnail.getThumbnail());
		} catch (Exception e) {
		}
	}
	
	public static byte[] createThumbnail(byte[] imagedata, int thumbWidth, int thumbHeight, int quality) {
		Image image = Toolkit.getDefaultToolkit().createImage(imagedata);
		MediaTracker mediaTracker = new MediaTracker(new Container());
		mediaTracker.addImage(image, 0);
		try {
			mediaTracker.waitForID(0);
		} catch (InterruptedException e) {
		}

		double thumbRatio = (double) thumbWidth / (double) thumbHeight;
		int imageWidth = image.getWidth(null);
		int imageHeight = image.getHeight(null);
		double imageRatio = (double) imageWidth / (double) imageHeight;
		if (thumbRatio < imageRatio) {
			thumbHeight = (int) (thumbWidth / imageRatio);
		} else {
			thumbWidth = (int) (thumbHeight * imageRatio);
		}

		ByteArrayOutputStream baos = null;
		BufferedOutputStream out = null;
		Graphics2D graphics2D = null;
		try {
			baos = new ByteArrayOutputStream();
			out = new BufferedOutputStream(baos);

			BufferedImage thumbImage = new BufferedImage(thumbWidth, thumbHeight, BufferedImage.TYPE_INT_RGB);
			graphics2D = thumbImage.createGraphics();
			graphics2D.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_SPEED);
			graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);

			graphics2D.drawImage(image, 0, 0, thumbWidth, thumbHeight, null);
			ImageIO.write(thumbImage, "JPG", out);

			return baos.toByteArray();
		} catch (Exception ex) {
			logger.fatal(ex, ex);
		} finally {
			if (baos != null) {
				try {
					baos.close();
				} catch (IOException e) {
				}
			}
			baos = null;
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
				}
			}
			out = null;
			if (graphics2D != null) {
				graphics2D.dispose();
			}
			graphics2D = null;
		}
		return new byte[0];
	}
	
	public static byte[] createThumbnail(String filename, int thumbWidth, int thumbHeight, int quality) {
		Image image = Toolkit.getDefaultToolkit().getImage(filename);
		MediaTracker mediaTracker = new MediaTracker(new Container());
		mediaTracker.addImage(image, 0);
		try {
			mediaTracker.waitForID(0);
		} catch (InterruptedException e) {
		}

		double thumbRatio = (double) thumbWidth / (double) thumbHeight;
		int imageWidth = image.getWidth(null);
		int imageHeight = image.getHeight(null);
		double imageRatio = (double) imageWidth / (double) imageHeight;
		if (thumbRatio < imageRatio) {
			thumbHeight = (int) (thumbWidth / imageRatio);
		} else {
			thumbWidth = (int) (thumbHeight * imageRatio);
		}

		ByteArrayOutputStream baos = null;
		BufferedOutputStream out = null;
		Graphics2D graphics2D = null;
		try {
			baos = new ByteArrayOutputStream();
			out = new BufferedOutputStream(baos);

			BufferedImage thumbImage = new BufferedImage(thumbWidth, thumbHeight, BufferedImage.TYPE_INT_RGB);
			graphics2D = thumbImage.createGraphics();
			graphics2D.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_SPEED);
			graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);

			graphics2D.drawImage(image, 0, 0, thumbWidth, thumbHeight, null);
			ImageIO.write(thumbImage, "JPG", out);

			return baos.toByteArray();
		} catch (Exception ex) {
			logger.fatal(ex, ex);
		} finally {
			if (baos != null) {
				try {
					baos.close();
				} catch (IOException e) {
				}
			}
			baos = null;
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
				}
			}
			out = null;
			if (graphics2D != null) {
				graphics2D.dispose();
			}
			graphics2D = null;
		}
		return new byte[0];
	}

	public static void createThumbnail(String filename, int thumbWidth, int thumbHeight, int quality, String outFilename) {
		Image image = Toolkit.getDefaultToolkit().getImage(filename);
		MediaTracker mediaTracker = new MediaTracker(new Container());
		mediaTracker.addImage(image, 0);
		try {
			mediaTracker.waitForID(0);
		} catch (InterruptedException e) {
		}

		double thumbRatio = (double) thumbWidth / (double) thumbHeight;
		int imageWidth = image.getWidth(null);
		int imageHeight = image.getHeight(null);
		double imageRatio = (double) imageWidth / (double) imageHeight;
		if (thumbRatio < imageRatio) {
			thumbHeight = (int) (thumbWidth / imageRatio);
		} else {
			thumbWidth = (int) (thumbHeight * imageRatio);
		}

		ByteArrayOutputStream baos = null;
		BufferedOutputStream out = null;
		Graphics2D graphics2D = null;
		try {
			baos = new ByteArrayOutputStream();
			out = new BufferedOutputStream(baos);

			BufferedImage thumbImage = new BufferedImage(thumbWidth, thumbHeight, BufferedImage.TYPE_INT_RGB);
			graphics2D = thumbImage.createGraphics();
			graphics2D.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_SPEED);
			graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);

			graphics2D.drawImage(image, 0, 0, thumbWidth, thumbHeight, null);
			ImageIO.write(thumbImage, "JPG", out);

			FileUtil.saveBytesToFile(outFilename, baos.toByteArray());
		} catch (Exception ex) {
			logger.fatal(ex, ex);
		} finally {
			if (baos != null) {
				try {
					baos.close();
				} catch (IOException e) {
				}
			}
			baos = null;
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
				}
			}
			out = null;
			if (graphics2D != null) {
				graphics2D.dispose();
			}
			graphics2D = null;
		}
	}

	public static void createThumbnail(byte[] data, int thumbWidth, int thumbHeight, int quality, String outFilename) {
		Image image = Toolkit.getDefaultToolkit().createImage(data);
		MediaTracker mediaTracker = new MediaTracker(new Container());
		mediaTracker.addImage(image, 0);
		try {
			mediaTracker.waitForID(0);
		} catch (InterruptedException e) {
		}

		double thumbRatio = (double) thumbWidth / (double) thumbHeight;
		int imageWidth = image.getWidth(null);
		int imageHeight = image.getHeight(null);
		double imageRatio = (double) imageWidth / (double) imageHeight;
		if (thumbRatio < imageRatio) {
			thumbHeight = (int) (thumbWidth / imageRatio);
		} else {
			thumbWidth = (int) (thumbHeight * imageRatio);
		}

		ByteArrayOutputStream baos = null;
		BufferedOutputStream out = null;
		Graphics2D graphics2D = null;
		try {
			baos = new ByteArrayOutputStream();
			out = new BufferedOutputStream(baos);

			BufferedImage thumbImage = new BufferedImage(thumbWidth, thumbHeight, BufferedImage.TYPE_INT_RGB);
			graphics2D = thumbImage.createGraphics();
			graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
			graphics2D.drawImage(image, 0, 0, thumbWidth, thumbHeight, null);

			ImageIO.write(thumbImage, "JPG", out);

			FileUtil.saveBytesToFile(outFilename, baos.toByteArray());
		} catch (Exception ex) {
			logger.fatal(ex, ex);
		} finally {
			if (baos != null) {
				try {
					baos.close();
				} catch (IOException e) {
				}
			}
			baos = null;
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
				}
			}
			out = null;
			if (graphics2D != null) {
				graphics2D.dispose();
			}
			graphics2D = null;
		}
	}

	public static byte[] toArray(BufferedImage image) {
		ByteArrayOutputStream baos = null;
		try {
			baos = new ByteArrayOutputStream();
			ImageIO.write(image, "JPG", baos);
			return baos.toByteArray();
		} catch (Exception ex) {
			logger.fatal(ex);
			return null;
		} finally {
			if (baos != null) {
				try {
					baos.close();
				} catch (IOException ex) {
				}
			}
			baos = null;
		}
	}
	
	public byte[] toArray() {
      BufferedImage bufferedImage = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_RGB);
		ByteArrayOutputStream baos = null;
		try {
			baos = new ByteArrayOutputStream();
			ImageIO.write(bufferedImage, "JPG", baos);
			return baos.toByteArray();
		} catch (Exception ex) {
			logger.fatal(ex);
			return null;
		} finally {
			if (baos != null) {
				try {
					baos.close();
				} catch (IOException ex) {
				}
			}
			baos = null;
		}
	}

	private static int[][] convertTo2DWithoutUsingGetRGB(BufferedImage image) {

		final byte[] pixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
		final int width = image.getWidth();
		final int height = image.getHeight();
		final boolean hasAlphaChannel = image.getAlphaRaster() != null;

		int[][] result = new int[height][width];
		if (hasAlphaChannel) {
			final int pixelLength = 4;
			for (int pixel = 0, row = 0, col = 0; pixel < pixels.length; pixel += pixelLength) {
				int argb = 0;
				argb += (((int) pixels[pixel] & 0xff) << 24); // alpha
				argb += ((int) pixels[pixel + 1] & 0xff); // blue
				argb += (((int) pixels[pixel + 2] & 0xff) << 8); // green
				argb += (((int) pixels[pixel + 3] & 0xff) << 16); // red
				result[row][col] = argb;
				col++;
				if (col == width) {
					col = 0;
					row++;
				}
			}
		} else {
			final int pixelLength = 3;
			for (int pixel = 0, row = 0, col = 0; pixel < pixels.length; pixel += pixelLength) {
				int argb = 0;
				argb += -16777216; // 255 alpha
				argb += ((int) pixels[pixel] & 0xff); // blue
				argb += (((int) pixels[pixel + 1] & 0xff) << 8); // green
				argb += (((int) pixels[pixel + 2] & 0xff) << 16); // red
				result[row][col] = argb;
				col++;
				if (col == width) {
					col = 0;
					row++;
				}
			}
		}

		return result;
	}

	public class Thumbnail {

		private int thumbWidth;
		private int thumbHeight;
		private String mime;
		private byte[] thumb;

		public Thumbnail(byte[] thumb, int thumbWidth, int thumbHeight) {
			this.thumb = thumb;
			this.thumbWidth = thumbWidth;
			this.thumbHeight = thumbHeight;
			this.mime = "application/octet-stream";
		}

		public Thumbnail(byte[] thumb, int thumbWidth, int thumbHeight, String mime) {
			this.thumb = thumb;
			this.thumbWidth = thumbWidth;
			this.thumbHeight = thumbHeight;
			this.mime = mime;
		}

		public byte[] getThumbnail() {
			return thumb;
		}

		public int getSize() {
			return thumb.length;
		}

		public int getWidth() {
			return thumbWidth;
		}

		public int getHeight() {
			return thumbHeight;
		}

		public String getMime() {
			return mime;
		}
	}
}
