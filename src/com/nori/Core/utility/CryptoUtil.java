package com.nori.Core.utility;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateCrtKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.EncryptedPrivateKeyInfo;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class CryptoUtil {

	private static PublicKey publicKey;
	private static PrivateKey privateKey;

	static {
		try {
			byte[] pubKey = ("-----BEGIN CERTIFICATE-----\n"
			  + "MIIFmzCCBIOgAwIBAgIEFdYSAzANBgkqhkiG9w0BAQsFADBSMQswCQYDVQQGEwJr\n"
			  + "cjEQMA4GA1UECgwHeWVzc2lnbjEVMBMGA1UECwwMQWNjcmVkaXRlZENBMRowGAYD\n"
			  + "VQQDDBF5ZXNzaWduQ0EgQ2xhc3MgMTAeFw0xNDAyMjExNTAwMDBaFw0xNTAyMjIx\n"
			  + "NDU5NTlaMHExCzAJBgNVBAYTAmtyMRAwDgYDVQQKDAd5ZXNzaWduMRQwEgYDVQQL\n"
			  + "DAtwZXJzb25hbDRJQjEMMAoGA1UECwwDU0hCMSwwKgYDVQQDDCPrr7zrs5HslYgo\n"
			  + "KTAwODgwNDMyMDA5MDcwMzE4ODAwMTEyODCCASIwDQYJKoZIhvcNAQEBBQADggEP\n"
			  + "ADCCAQoCggEBAIBcEh0W9hfihMOda588w53sf+i2gmARdb9D2sDJWwceUSnAnp55\n"
			  + "K/0raENPGuFa4wF2BDBmYD/LpNz8qlwPy/Dv40PDf0+EdlPsgvXJkhwTMaxAaiB3\n"
			  + "/MoLpJbRElAhAUp0uilYryUy+b3lO6YriN54aEOjtqxzTY5xORUJB5wZe2Js7q+5\n"
			  + "kJqwnUHM5/85UctPzIpsDu725eDjlzlL92gglTbbuDVLx2h8QXeypTGdW4fvtCzA\n"
			  + "HIJlQUOF6igwrBlilJlFnsNOi3XVvtbTJNrS0DX7Tkgrske5IjDtiR38Zx//hhqr\n"
			  + "Tel+iitrAi8QeFk28WDzq8EcrYwfxP+G3I8CAwEAAaOCAlgwggJUMIGPBgNVHSME\n"
			  + "gYcwgYSAFFIEMp+PnSFyuvozmKhhficzJI1foWikZjBkMQswCQYDVQQGEwJLUjEN\n"
			  + "MAsGA1UECgwES0lTQTEuMCwGA1UECwwlS29yZWEgQ2VydGlmaWNhdGlvbiBBdXRo\n"
			  + "b3JpdHkgQ2VudHJhbDEWMBQGA1UEAwwNS0lTQSBSb290Q0EgNIICEAMwHQYDVR0O\n"
			  + "BBYEFC6+ozThFqD+WrwoFBUxoQBwuKJQMA4GA1UdDwEB/wQEAwIGwDB5BgNVHSAB\n"
			  + "Af8EbzBtMGsGCSqDGoyaRQEBBDBeMC4GCCsGAQUFBwICMCIeIMd0ACDHeMmdwRyy\n"
			  + "lAAgrPXHeMd4yZ3BHAAgx4WyyLLkMCwGCCsGAQUFBwIBFiBodHRwOi8vd3d3Lnll\n"
			  + "c3NpZ24ub3Iua3IvY3BzLmh0bTBoBgNVHREEYTBfoF0GCSqDGoyaRAoBAaBQME4M\n"
			  + "CeuvvOuzkeyViDBBMD8GCiqDGoyaRAoBAQEwMTALBglghkgBZQMEAgGgIgQgcS3y\n"
			  + "rA4YvzrQOR8kLzwtrl31J8b/G5n0b/Qvbp81qjMwcgYDVR0fBGswaTBnoGWgY4Zh\n"
			  + "bGRhcDovL2RzLnllc3NpZ24ub3Iua3I6Mzg5L291PWRwNHAzNTM5MSxvdT1BY2Ny\n"
			  + "ZWRpdGVkQ0Esbz15ZXNzaWduLGM9a3I/Y2VydGlmaWNhdGVSZXZvY2F0aW9uTGlz\n"
			  + "dDA4BggrBgEFBQcBAQQsMCowKAYIKwYBBQUHMAGGHGh0dHA6Ly9vY3NwLnllc3Np\n"
			  + "Z24ub3JnOjQ2MTIwDQYJKoZIhvcNAQELBQADggEBABen3UisYLYKjc6HMkrt8Vmn\n"
			  + "3sgLuEr8xE4l6FE6qUUEiTEt/SUukEA03wSKxV9fwyzv3mHi/Y6PZxio65mH6jDv\n"
			  + "wbJXUVM180PTTXoIgr9pPafr/jqsmcb0tX3aNvRGeH73DYgCqeu43d3qKZt5ToHh\n"
			  + "TRepfbh3cjfjt3HqdGQXRKYwtHq6oyV8xbmHWa+79Lsg7IgqMo6gT4aYQD9OdVBa\n"
			  + "xKkP7MuswcpViFxjG+EZaVpVy/YPwy0YcM948jz6QUi5r/gLXTC5LfEhbHaPw2Z5\n"
			  + "2N6pWoUtRW8vZFHiGzjtgs5ps9chRuca8aSF1ncHFbCIG0Hj9wND4w8PAbTg6z4=\n"
			  + "-----END CERTIFICATE-----").getBytes();
			
			CertificateFactory certificateFactory = CertificateFactory.getInstance("X509");
			X509Certificate cert = (X509Certificate) certificateFactory.generateCertificate(new ByteArrayInputStream(pubKey));
			publicKey = cert.getPublicKey();

			byte[] priKey = HexUtil.toBytes("30820510301a06082a831a8c9a44010f300e04084aa5ef13a7acd1d602020800048204f047c4c0970b658ab749376632cad40df40d61d877bef5aec498d7e033b3059f257f4cb583761038d3ae946731e1704ec71273bcf3e21f4325ce75392f1daf982471d4d04bf70f6a69708b338ccb0f27bf06abfc7af4f4725f3a7bfcf1bddedcc76905d29d87f992ca0da54ef1b8ad3f725286d95dcd268865bc86d085c6521014724f044cd2294fbecda573a0bc199fbc57e3171e2a5e61dd416f94ec656a8862eea2097eb8f466534ef9df7f26db71b17432833d930fd63b4b270ffdf6ed28262280afcbd15b0a429e5a50958bf2cbf5db3bdfe8dd410e14fd966788d576d2a07d40824e9256631520cca7465a00e3794511710a99ea58cc2830e3b1ad67b9b0c50ebc8844ee36628d1e81c3087eb3f236ae5a542770831d286f5423cac2c407d1ead5b03fad2926b4ca33cd7aec78fb7f52d9d4dff76a2647df3b01bb3fdf25c42ea0efda598de32d3910c0f83034fe942ecab109adc0a5aeb2262667a146851c96f9f401078ba00629905b791f7ab16d89f33667d1005c33ad65965b287f2c82eb64ac9155586548ce21461abe29ded74147400ddaf6b82a0141679037137181b9baf02f04a54a70129b7b858cb2e448512fe3814c000f9619187df72e005d63c7eb12095e83ef725399962d6c8d2d1ffb6a9064874ab356a05cc674c219797e19ca6a13260166996a8e5f20f8716df516e479d5881bb0f8a31c2dd6221b4f8c4254c8b327753b27aff3212f28dbd7a90ddd437d8c0f359380b516d1e864041a0744928cf35d40ddb2e232888270f10e101578a855a53a88dbf7362f80b7ccf76d854191242263eebcc47984d76208887c731736562b1d1fe2f34418a0b7c689d2d5b72dd2d554b6eea8a9d956c6d9db276437a239145ddb0ac609fb02417aa99b042af0ffddf42018f135abd4f1dff17a581d3115901b0aa57d08f1282ca641cc423111248c5f1df92ac81a72adfb75185b345b9c675f31894459929a2fd1d7a00862d2ab0f4d5f8ce593099b57c643f4de2744dd874f9fb33d38c53bd8187c31ff466316caffd571b37409b28893fc7cbdbf7723a3f34c7bf6d13d25edc8ad7636313c13f2c0c732d814333b038f907b609f347efb9dbc8df36ee90c9cc3c244460c192ebd32253371fafd632c0fc27d4a11075e3fe38152eba1d55ebb750ec0489c18f1b5d67880ef3a795a3115b2e65130af38269e92c8067841e0ae7aa1f8cb8de418b0fda42929a63c738c8298a56255188da735bd044ee81c0dea173cbf8efbd0d3ecfae05771c812408f87b5335ebfd945f0594b06fbd4368f73b5a9d27995abb7a436ddd297f0718a5cc71df8697b9244c3e37aeacfb7af2f79e857b4c42aae67f9f101d0a88bfbebeee647c76bddfc546834e3b272da0c979fb76822115c30577531574be38031a823d38c66058d4365ed66fd7da4ad8ad5bcde5e5c2e7d3d529a7c5538749d27e501840201b9829a7a231ba92420abf525c3c8801c300d1920dbb29361198f91bd2f134fa60afff8c7c93aef610eea0057dffb711f100bda817f02944f6166feff179063783c4fb69f77e49f065b26f088373f1ed5fe6be15b00034e4cdc6d52fa2bfaf142e3f3b4d7af8fc81514b8ee90611f6381e675f50effa3c9c139bcd136eff625692cefaaf49e5452876006b3f456142c5c7c7cfc04a2c48b10056d1d675a00f3843c7432218b26a94faae6df758b2ef360555d92b320f006f512d6e2db31b282d4864697050cefb7abcb80fa7383ac05ac8609f9d0481f5ddb283f4b16348b");
			
			privateKey = getPrivateKey(new ByteArrayInputStream(priKey), "min002004");
		} catch (Exception ex) {
			Logger.getLogger(CryptoUtil.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public static final String RSA = "RSA";
	private static final String HASH = "SHA1";

	private static byte[] getHash(byte[] input) {
		try {
			MessageDigest md = MessageDigest.getInstance(HASH);
			return md.digest(input);
		} catch (NoSuchAlgorithmException ex) {
			Logger.getLogger(CryptoUtil.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;
	}

	private static int toInt(byte[] src, int srcPos) {
		int dword = 0;
		for (int i = 0; i < 4; i++) {
			dword = (dword << 8) + (src[i + srcPos] & 0xFF);
		}
		return dword;
	}

	public static String encrypt(String plain) {
		try {
			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			cipher.init(Cipher.ENCRYPT_MODE, publicKey);
			return Base64.Encode(cipher.doFinal(plain.getBytes("UTF-8")), false);
		} catch (Exception ex) {
			Logger.getLogger(CryptoUtil.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;
	}
	
	public static String decrypt(String encrypted, boolean tolerance) {
		try {
			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			cipher.init(Cipher.DECRYPT_MODE, privateKey);
			return new String(cipher.doFinal(Base64.Decode(encrypted)), "UTF-8");
		} catch (Exception ex) {
			Logger.getLogger(CryptoUtil.class.getName()).log(Level.SEVERE, null, ex);
		}
		if (tolerance == true) {
			return encrypted;
		} else {
			return null;
		}
	}
	
	public static String encryptWithPrivateKey(String plain) {
		try {
			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			cipher.init(Cipher.ENCRYPT_MODE, privateKey);
			return Base64.Encode(cipher.doFinal(plain.getBytes("UTF-8")), false);
		} catch (Exception ex) {
			Logger.getLogger(CryptoUtil.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;
	}
	
	public static String decryptWithPublicKey(String encrypted) {
		try {
			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			cipher.init(Cipher.DECRYPT_MODE, publicKey);
			return new String(cipher.doFinal(Base64.Decode(encrypted)), "UTF-8");
		} catch (Exception ex) {
			Logger.getLogger(CryptoUtil.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;
	}

	private static PrivateKey getPrivateKey(InputStream fis, String password) throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException, InvalidAlgorithmParameterException {
		byte[] encodedKey = null;
		ByteArrayOutputStream bos = null;
		try {
			bos = new ByteArrayOutputStream();
			byte[] buffer = new byte[1024];
			int read = -1;
			while ((read = fis.read(buffer)) != -1) {
				bos.write(buffer, 0, read);
			}
			encodedKey = bos.toByteArray();
		} finally {
			if (bos != null) {
				try {
					bos.close();
				} catch (IOException ie) {
				}
			}
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException ie) {
				}
			}
		}

		EncryptedPrivateKeyInfo encryptedPrivateKeyInfo = new EncryptedPrivateKeyInfo(encodedKey);

		byte[] salt = new byte[8];
		System.arraycopy(encodedKey, 20, salt, 0, 8);

		byte[] cBytes = new byte[4];
		System.arraycopy(encodedKey, 30, cBytes, 2, 2);
		int iterationCount = toInt(cBytes, 0);

		byte[] dk = pbkdf1(password, salt, iterationCount);

		byte[] keyData = new byte[16];
		System.arraycopy(dk, 0, keyData, 0, 16);

		byte[] div = new byte[20];
		byte[] tmp4Bytes = new byte[4];
		System.arraycopy(dk, 16, tmp4Bytes, 0, 4);
		div = getHash(tmp4Bytes);
		byte[] iv = new byte[16];
		System.arraycopy(div, 0, iv, 0, 16);

		BouncyCastleProvider provider = new BouncyCastleProvider();
		Cipher cipher = Cipher.getInstance("SEED/CBC/PKCS5Padding", provider);
		Key key = new SecretKeySpec(keyData, "SEED");
		cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv));
		byte[] output = cipher.doFinal(encryptedPrivateKeyInfo.getEncryptedData());

		PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(output);
		KeyFactory keyFactory = KeyFactory.getInstance(RSA);
		return (RSAPrivateCrtKey) keyFactory.generatePrivate(keySpec);
	}

	private static byte[] pbkdf1(String password, byte[] salt, int iterationCount) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("SHA1");
		md.update(password.getBytes());
		md.update(salt);
		byte[] dk = md.digest();
		for (int i = 1; i < iterationCount; i++) {
			dk = md.digest(dk);
		}
		return dk;
	}
}
