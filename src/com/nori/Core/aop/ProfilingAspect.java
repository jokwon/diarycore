package com.nori.Core.aop;

import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.util.StopWatch;

@Aspect
public class ProfilingAspect {

	@Pointcut("within(@org.springframework.stereotype.Repository *)")
	public void daoMethods() {
	}

	@Pointcut("within(@org.springframework.stereotype.Service *)")
	public void serviceMethods() {
	}

	@Pointcut("within(@org.springframework.stereotype.Controller *)")
	public void controllerMethods() {
	}

	@Pointcut("daoMethods() || serviceMethods() || controllerMethods()")
	public void profileMethods() {
	}

	@Around("daoMethods()")
	public Object profileDao(ProceedingJoinPoint jonPoint) throws Throwable {
		Logger logger = Logger.getLogger(jonPoint.getTarget().getClass().getName());
		Signature signature = jonPoint.getSignature();
		StopWatch stopWatch = null;
		try {
			if (logger.isTraceEnabled()) {
				stopWatch = new StopWatch(signature.toShortString());
				stopWatch.start(jonPoint.getSignature().getName());
			}
			Object result = jonPoint.proceed();
			return result;
		} finally {
			if (logger.isTraceEnabled()) {
				stopWatch.stop();
				logger.trace(stopWatch.prettyPrint());
				//메롱~!!
			}
		}
	}

	@Around("serviceMethods()")
	public Object profileService(ProceedingJoinPoint jonPoint) throws Throwable {
		Logger logger = Logger.getLogger(jonPoint.getTarget().getClass().getName());
		Signature signature = jonPoint.getSignature();
		StopWatch stopWatch = null;
		try {
			if (logger.isTraceEnabled()) {
				stopWatch = new StopWatch(signature.toShortString());
				stopWatch.start(jonPoint.getSignature().getName());
			}
			Object result = jonPoint.proceed();
			return result;
		} finally {
			if (logger.isTraceEnabled()) {
				stopWatch.stop();
				logger.trace(stopWatch.prettyPrint());
			}
		}
	}

	@Around("controllerMethods()")
	public Object profileController(ProceedingJoinPoint jonPoint) throws Throwable {
		Logger logger = Logger.getLogger(jonPoint.getTarget().getClass().getName());
		Signature signature = jonPoint.getSignature();
		StopWatch stopWatch = null;
		try {
			if (logger.isTraceEnabled()) {
				stopWatch = new StopWatch(signature.toShortString());
				stopWatch.start(jonPoint.getSignature().getName());
			}
			Object result = jonPoint.proceed();
			return result;
		} finally {
			if (logger.isTraceEnabled()) {
				stopWatch.stop();
				logger.trace(stopWatch.prettyPrint());
			}
		}
	}
}
