package com.nori.Core.domain;

import java.io.Serializable;

import org.apache.ibatis.type.Alias;

@Alias("userRole")
public class UserRole implements Serializable {

	private static final long serialVersionUID = 4610327208166153941L;

	private String roleCode;

	private String roleName;

	private int roleType;

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public int getRoleType() {
		return roleType;
	}

	public void setRoleType(int roleType) {
		this.roleType = roleType;
	}

}
