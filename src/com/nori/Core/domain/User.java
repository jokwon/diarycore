package com.nori.Core.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.type.Alias;

import com.nori.Core.form.Pagination;

@Alias("user")
public class User extends Pagination implements Serializable {
	
	private static final long serialVersionUID = -8369712541698922901L;
	
	
	private int uId;
	
	private int id;
	
	private String userId;

	private String password;

	private String birthDay;
	
	private String name;

	private String phone;

	private String part;
	
	private String team;
	
	private String email;

	private Date regDate;

	private Date upDate;
	
	private String address;
	
	private String employeeDate;

	private UserAccessLog userAccessLog;                        

	private List<UserRoles> userRoleList;
	
	private String token;

	private String photo;
	
	private int followCount;
	
	private int followerCount;
	
	private int myFollowCheck;
	
	private int iDisplayStart;
	
	private int 	iDisplayLength;
		
	private int code; 
	
	private String position;
	
	private String duty;
	
	private int companyId;
	
	private String sSearch;
	
	public String getsSearch() {
		return sSearch;
	}

	public void setsSearch(String sSearch) {
		this.sSearch = sSearch;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getDuty() {
		return duty;
	}

	public void setDuty(String duty) {
		this.duty = duty;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getRegDate() {
		return regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

	public Date getUpDate() {
		return upDate;
	}

	public void setUpDate(Date upDate) {
		this.upDate = upDate;
	}

	public UserAccessLog getUserAccessLog() {
		return userAccessLog;
	}

	public void setUserAccessLog(UserAccessLog userAccessLog) {
		this.userAccessLog = userAccessLog;
	}

	public List<UserRoles> getUserRoleList() {
		return userRoleList;
	}

	public void setUserRoleList(List<UserRoles> userRoleList) {
		this.userRoleList = userRoleList;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public int getFollowCount() {
		return followCount;
	}

	public void setFollowCount(int followCount) {
		this.followCount = followCount;
	}

	public int getFollowerCount() {
		return followerCount;
	}

	public void setFollwerCount(int followerCount) {
		this.followerCount = followerCount;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}
	public int getMyFollowCheck() {
		return myFollowCheck;
	}
	
	public void setMyFollowCheck(int myFollowCheck) {
		this.myFollowCheck = myFollowCheck;
	}

	public int getiDisplayStart() {
		return iDisplayStart;
	}

	public void setiDisplayStart(int iDisplayStart) {
		this.iDisplayStart = iDisplayStart;
	}

	public int getiDisplayLength() {
		return iDisplayLength;
	}

	public void setiDisplayLength(int iDisplayLength) {
		this.iDisplayLength = iDisplayLength;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPart() {
		return part;
	}

	public void setPart(String part) {
		this.part = part;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setFollowerCount(int followerCount) {
		this.followerCount = followerCount;
	}

	public int getuId() {
		return uId;
	}

	public void setuId(int uId) {
		this.uId = uId;
	}

	public String getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(String birthDay) {
		this.birthDay = birthDay;
	}

	public String getEmployeeDate() {
		return employeeDate;
	}

	public void setEmployeeDate(String employeeDate) {
		this.employeeDate = employeeDate;
	}



	
	
	
}
