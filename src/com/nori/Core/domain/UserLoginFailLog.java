package com.nori.Core.domain;

import java.util.Date;

import org.apache.ibatis.type.Alias;

@Alias("userLoginFailLog")
public class UserLoginFailLog {

	private Date tryDate;

	private long tryNo;

	private String userId;

	private String accessIp;

	private String userAgent;

	private String locale;

	private String country;

	private String referer;

	private Date tryDateTime;

	public Date getTryDate() {
		return tryDate;
	}

	public void setTryDate(Date tryDate) {
		this.tryDate = tryDate;
	}

	public long getTryNo() {
		return tryNo;
	}

	public void setTryNo(long tryNo) {
		this.tryNo = tryNo;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAccessIp() {
		return accessIp;
	}

	public void setAccessIp(String accessIp) {
		this.accessIp = accessIp;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getReferer() {
		return referer;
	}

	public void setReferer(String referer) {
		this.referer = referer;
	}

	public Date getTryDateTime() {
		return tryDateTime;
	}

	public void setTryDateTime(Date tryDateTime) {
		this.tryDateTime = tryDateTime;
	}

}
