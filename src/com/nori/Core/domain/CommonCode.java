package com.nori.Core.domain;

public class CommonCode {
	
	/*
	 * 방문구분
	 */
	public enum VisitDiv{
		A("1","격월"),
		B("2","매월"),
		C("3","분기"),
		D("4","상주");
		
		String code;
		String value;
		
		VisitDiv(String code, String value){
			this.code = code;
			this.value = value;
		}
		
		public String getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
		
		public static String getValue(String Code) {
			for (VisitDiv visitDiv : VisitDiv.values()) {
				if (Code!=null && Code.equals(visitDiv.getCode())) {
					return visitDiv.getValue();
				}
			}
			return null;
		}
		
	}
	
	/*
	 * 점검구분
	 */
	public enum InspectionDiv{
		
		A("1","작동"),
		B("2","종합"),
		C("3","종합+작동");
		
		String code;
		String value;
		
		InspectionDiv(String code, String value){
			this.code = code;
			this.value = value;
		}
		
		public String getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
		
		public static String getValue(String Code) {
			for (InspectionDiv inspectionDiv : InspectionDiv.values()) {
				if (Code!=null && Code.equals(inspectionDiv.getCode())) {
					return inspectionDiv.getValue();
				}
			}
			return null;
		}
	}
	
	/*
	 * 종합구분
	 */
	public enum SynthesisDiv{
		A("1","무"),
		B("2","공공종합"),
		C("3","학교종합"),
		D("4","다중종합"),
		E("5","아파트종합"),
        F("6","오전종합"),
        G("7","일반종합");
		
		String code;
		String value;
		
		SynthesisDiv(String code, String value){
			this.code = code;
			this.value = value;
		}
		
		public String getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
		
		public static String getValue(String Code) {
			for (SynthesisDiv synthesisDiv : SynthesisDiv.values()) {
				if (Code!=null && Code.equals(synthesisDiv.getCode())) {
					return synthesisDiv.getValue();
				}
			}
			return null;
		}
	}
	
	/*
	 * 소방안전관리자_보조구분
	 */
	public enum FireSafetyManagerSupport{
		
		A("1","비대상"),
		B("2","대상");
		
		String code;
		String value;
		
		FireSafetyManagerSupport(String code, String value){
			this.code = code;
			this.value = value;
		}
		
		public String getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
		
		public static String getValue(String Code) {
			for (FireSafetyManagerSupport fireSafetyManagerSupport : FireSafetyManagerSupport.values()) {
				if (Code!=null && Code.equals(fireSafetyManagerSupport.getCode())) {
					return fireSafetyManagerSupport.getValue();
				}
			}
			return null;
		}
		
	}
	
	/*
	 * 소방안전관리자_구분
	 */
	public enum FireSafetyManagerDivison{
		
		B("2","일반"),
		A("1","공공");
		
		String code;
		String value;
		
		FireSafetyManagerDivison(String code, String value){
			this.code = code;
			this.value = value;
		}
		
		public String getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
		
		public static String getValue(String Code) {
			for (FireSafetyManagerDivison fireSafetyManagerDivison : FireSafetyManagerDivison.values()) {
				if (Code!=null && Code.equals(fireSafetyManagerDivison.getCode())) {
					return fireSafetyManagerDivison.getValue();
				}
			}
			return null;
		}
		
	}
	
	/*
	 * 소방안전관리자_급수
	 */
	public enum FireSafetyManagerGrade{
		
		A("1","3급"),
		B("2","2급"),
		C("3","1급"),
		D("4","특수"),
		E("5","비대상");
		
		String code;
		String value;
		
		FireSafetyManagerGrade(String code, String value){
			this.code = code;
			this.value = value;
		}
		
		public String getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
		
		public static String getValue(String Code) {
			for (FireSafetyManagerGrade fireSafetyManagerGrade : FireSafetyManagerGrade.values()) {
				if (Code!=null &&  Code.equals(fireSafetyManagerGrade.getCode())) {
					return fireSafetyManagerGrade.getValue();
				}
			}
			return null;
		}
		
	}
	
	/*
	 * 성별
	 */
	public enum Gender{
		
		A("1","남성"),
		B("2","여성");
		
		String code;
		String value;
		
		Gender(String code, String value){
			this.code = code;
			this.value = value;
		}
		
		public String getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
		
		public static String getValue(String Code) {
			for (Gender gender : Gender.values()) {
				if (Code!=null && Code.equals(gender.getCode())) {
					return gender.getValue();
				}
			}
			return null;
		}
		
	}
	
	
	/*
	 * 업무담당자_건물주와의관
	 */
	public enum OwnerRelation{
		
		A("1","건물주"),
		B("2","동대표"),
		C("3","총무"),
		D("4","건축주"),
		E("5","용역"),
		F("6","기타");
		
		String code;
		String value;
		
		OwnerRelation(String code, String value){
			this.code = code;
			this.value = value;
		}
		
		public String getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
		
		public static String getValue(String Code) {
			for (OwnerRelation ownerRelation : OwnerRelation.values()) {
				if (Code!=null && Code.equals(ownerRelation.getCode())) {
					return ownerRelation.getValue();
				}
			}
			return null;
		}
		
	}
	
	/*
	 * 업무담당자_구분
	 */
	public enum OperaterDivision{
		
		A("O","업무담당자"),
		B("P","결제담당자");
	
		String code;
		String value;
		
		OperaterDivision(String code, String value){
			this.code = code;
			this.value = value;
		}
		
		public String getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
		
		public static String getValue(String Code) {
			for (OperaterDivision operaterDivision : OperaterDivision.values()) {
				if (Code!=null && Code.equals(operaterDivision.getCode())) {
					return operaterDivision.getValue();
				}
			}
			return null;
		}
		
	}
	
	/*
	 * 업무담당자_구분
	 */
	public enum ReceiverKind{
		
		A("P","P형"),
		B("R","R형");
	
		String code;
		String value;
		
		ReceiverKind(String code, String value){
			this.code = code;
			this.value = value;
		}
		
		public String getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
		
		public static String getValue(String Code) {
			for (ReceiverKind receiverKind : ReceiverKind.values()) {
				if (receiverKind.getCode() == Code) {
					return receiverKind.getValue();
				}
			}
			return null;
		}
		
	}
	
	/*
	 * 계약_구분
	 */
	public enum ContractDiv{
		
		A("1","소방안전관리"),
		B("2","소방시설점검"),
		C("3","소방시설공사"),
		D("4","기타");
	
		private String code;
		private String value;
		
		ContractDiv(String code, String value){
			this.code = code;
			this.value = value;
		}
		
		public String getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
		
		public static String getValue(String Code) {
			for (ContractDiv contractDiv : ContractDiv.values()) {
				if (Code!=null && Code.equals(contractDiv.getCode())) {
					return contractDiv.getValue();
				}
			}
			return null;
		}
		
	}
	
	
	public enum UseagePoint{
		
		A("1.2","수련시설"),
		B("1.2","노유자시설"),
		C("1.2","숙박시설"),
		D("1.2","위락시설"),
		E("1.2","의료시설(정신보건의료기간)"),
		F("1.2","복합건축물(1류에 속하는 시설이 있는 경우)"),
		G("1.1","교정및군사시설(군사시설제외)"),
	    H("1.1","지하가"),
		I("1.1","복합건축물(1류에 속하는 시설이 있는 경우 제외)"),
		J("1.1","종교시설"),
		K("1.1","발전시설"),
		L("1.1","판매시설"),
		M("1.1","문화및집회시설"),
		N("1.1","의료시설(정신보건시설제외)"),
		O("1.0","업무시설"),
		P("1.0","방송통신시설"),
		Q("1.0","운수시설"),
		R("1.0","운동시설"),
		S("1.0","공공기관"),
		T("1.0","근린생활시설"),
		U("0.9","공장"),
		V("0.9","창고시설"),
		W("0.9","위험물저장및처리시설"),
		X("0.8","공동주택"),
		Y("0.8","교육연구시설"),
		Z("0.8","관광휴게시설"),
	    A1("0.8","동물및식물관련시설"),
	    B1("0.8","분뇨및쓰레기처리시설(자원순환관련시설)"),
	    C1("0.8","지하구"),
	    D1("0.8","문화재"),
	    E1("0.8","항공기및자동차관련시설"),
	    F1("0.8","묘지관련시설"),
	    G1("0.8","장례시설"),
	    H1("0.8","군사시설");
		
	
		private String code;
		private String value;
		
		UseagePoint(String code, String value){
			this.code = code;
			this.value = value;
		}
		
		public String getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
		
		public static String getValue(String Code) {
			for (UseagePoint useagePoint : UseagePoint.values()) {
				if (Code!=null && Code.equals(useagePoint.getCode())) {
					return useagePoint.getValue();
				}
			}
			return null;
		}
		
	}
	public enum Appointment{
		
		A("1","대행선임"),
		B("2","자체선임");
		
		
		private String code;
		private String value;
		
		Appointment(String code, String value){
			this.code = code;
			this.value = value;
		}
		
		public String getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
		
		public static String getValue(String Code) {
			for (Appointment appointment : Appointment.values()) {
				if (Code!=null && Code.equals(appointment.getCode())) {
					return appointment.getValue();
				}
			}
			return null;
		}
		
	}
	//해지사유
	public enum Reason{
		
		A("1","업체변경"),
		B("2","자체선임"),
		C("3","금액불만"),
		D("4","지적불만"),
		E("5","업무불만"),
		F("6","수금불만"),
		G("7","건물주/소장변경"),
		H("8","계약만료"),
		I("9","일방해지"),
		J("10","기타");
		
		
		
		private String code;
		private String value;
		
		Reason(String code, String value){
			this.code = code;
			this.value = value;
		}
		
		public String getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
		
		public static String getValue(String Code) {
			for (Reason reason : Reason.values()) {
				if (Code!=null && Code.equals(reason.getCode())) {
					return reason.getValue();
				}
			}
			return null;
		}
		
	}
	public enum Demander{
		
		A("1","고객요청"),
		B("2","당사해지"),
		C("3","기타");
		
		
		
		private String code;
		private String value;
		
		Demander(String code, String value){
			this.code = code;
			this.value = value;
		}
		
		public String getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
		
		public static String getValue(String Code) {
			for (Demander demander : Demander.values()) {
				if (Code!=null && Code.equals(demander.getCode())) {
					return demander.getValue();
				}
			}
			return null;
		}
		
	}
	//고객해지통보타입
	public enum CancelNotice{
		
		A("1","팩스통보"),
		B("2","우편통보"),
		C("3","내용증명"),
		D("4","지급명령"),
		E("5","전화해지"),
		F("6","기타");
		
		
		
		
		
		private String code;
		private String value;
		
		CancelNotice(String code, String value){
			this.code = code;
			this.value = value;
		}
		
		public String getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
		
		public static String getValue(String Code) {
			for (CancelNotice cancelNotice : CancelNotice.values()) {
				if (Code!=null && Code.equals(cancelNotice.getCode())) {
					return cancelNotice.getValue();
				}
			}
			return null;
		}
		
	}
	//소방서해지통보타입
	public enum FireStationNoticeType{
		
		A("1","팩스통보"),
		B("2","우편통보"),
		C("3","전화통보"),
		D("4","내용증명"),
		E("5","기타");
		
		
		private String code;
		private String value;
		
		FireStationNoticeType(String code, String value){
			this.code = code;
			this.value = value;
		}
		
		public String getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
		
		public static String getValue(String Code) {
			for (FireStationNoticeType fireStationNoticeType : FireStationNoticeType.values()) {
				if (Code!=null && Code.equals(fireStationNoticeType.getCode())) {
					return fireStationNoticeType.getValue();
				}
			}
			return null;
		}
		
	}
	//소방서해지통보타입
	public enum UnPaidMonthly{
		
		A("1","1 개월"),
		B("2","2 개월"),
		C("3","3 개월"),
		D("4","4 개월"),
		E("5","5 개월"),
		F("6","6 개월"),
		G("7","7 개월"),
		H("8","8 개월"),
		I("9","9 개월"),
		J("10","10 개월"),
		K("11","11 개월"),
		L("12","12 개월");
		
		
		private String code;
		private String value;
		
		UnPaidMonthly(String code, String value){
			this.code = code;
			this.value = value;
		}
		
		public String getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
		
		public static String getValue(String Code) {
			for (UnPaidMonthly unPaidMonthly : UnPaidMonthly.values()) {
				if (Code!=null && Code.equals(unPaidMonthly.getCode())) {
					return unPaidMonthly.getValue();
				}
			}
			return null;
		}
		
	}
	public enum Roles{
		
		A("ROLE_ADMIN","관리자"),
		B("ROLE_USER","일반사용자");
		
		
		private String code;
		private String value;
		
		Roles(String code, String value){
			this.code = code;
			this.value = value;
		}
		
		public String getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
		
		public static String getValue(String Code) {
			for (Roles roles : Roles.values()) {
				if (Code!=null && Code.equals(roles.getCode())) {
					return roles.getValue();
				}
			}
			return null;
		}
		
	}
	
	
	
	
	
}
