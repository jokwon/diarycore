package com.nori.Core.domain;

import java.io.Serializable;

import org.apache.ibatis.type.Alias;

@Alias("userRoles")
public class UserRoles implements Serializable {

	private static final long serialVersionUID = -3234048769417481033L;

	private String userId;

	private String roleCode;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	@Override
	public String toString() {
		return "UserRoles [userId=" + userId + ", roleCode=" + roleCode + "]";
	}

}
