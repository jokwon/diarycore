package com.nori.Core.domain;

import java.util.Date;

public class FileUpload {

	private int id;
	private String orgName;
	private String newName;
	private String ext;
	private long size;
	private Date regDate;
	private String dbUploadPath;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public String getNewName() {
		return newName;
	}
	public void setNewName(String newName) {
		this.newName = newName;
	}
	public String getExt() {
		return ext;
	}
	public void setExt(String ext) {
		this.ext = ext;
	}
	public long getSize() {
		return size;
	}
	public void setSize(long size) {
		this.size = size;
	}
	public Date getRegDate() {
		return regDate;
	}
	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}
	public String getDbUploadPath() {
		return dbUploadPath;
	}
	public void setDbUploadPath(String dbUploadPath) {
		this.dbUploadPath = dbUploadPath;
	}
	
	
}
