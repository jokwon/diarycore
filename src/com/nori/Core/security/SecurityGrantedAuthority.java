
package com.nori.Core.security;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.SpringSecurityCoreVersion;

import com.nori.Core.domain.User;
import com.nori.Core.domain.UserRoles;

public class SecurityGrantedAuthority implements GrantedAuthority {

	public enum Type {
		SUPER_ADMIN(1, "SUPER_ADMIN"), 
		ADMIN(2, "ADMIN"), 
		USER(3, "USER");

		private int code;
		private String value;

		Type(int code, String value) {
			this.code = code;
			this.value = value;
		}

		public int getCode() {
			return code;
		}

		public String getValue() {
			return value;
		}

		public boolean isMatched(Type type) {
			if (this.equals(type)) {
				return true;
			}
			return false;
		}

		public static Type toType(int value) {
			for (Type type : Type.values()) {
				if (type.getCode() == value) {
					return type;
				}
			}
			return null;
		}

		public static Type fromValue(String value) {
			for (Type type : Type.values()) {
				if (type.value.equalsIgnoreCase(value)) {
					return type;
				}
			}
			return null;
		}
	}

	private static final long serialVersionUID = SpringSecurityCoreVersion.SERIAL_VERSION_UID;
	private static final List<GrantedAuthority> authorities;
	private final String role;
	private final String name;
	private final Type type;

	static {
		authorities = new ArrayList<>();
	}

	public SecurityGrantedAuthority(String role, String name, Type type) {

		this.role = role;
		this.name = name;
		this.type = type;

		addRole(this);
	}

	private static void addRole(SecurityGrantedAuthority role) {
		synchronized (authorities) {
			authorities.add(role);
		}
	}

	public String getAuthority() {
		return role;
	}

	public String getName() {
		return name;
	}

	public Type getType() {
		return type;
	}

	public String getRole() {
		return role;
	}

	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (obj instanceof SecurityGrantedAuthority) {
			return role.equals(((SecurityGrantedAuthority) obj).role);
		}

		return false;
	}

	public int hashCode() {
		return this.role.hashCode();
	}

	public String toString() {
		return this.role;
	}

	public static List<GrantedAuthority> getRoles() {
		return Collections.unmodifiableList(authorities);
	}

	public static GrantedAuthority[] getArray() {
		GrantedAuthority[] roles = new GrantedAuthority[authorities.size()];
		return Collections.unmodifiableList(authorities).toArray(roles);
	}

	public static List<GrantedAuthority> getTypeMatchedRoles(int typeValue) {
		Type type = Type.toType(typeValue);
		List<GrantedAuthority> roles = new ArrayList<GrantedAuthority>();
		Iterator<GrantedAuthority> iterator = authorities.iterator();
		while (iterator.hasNext()) {
			SecurityGrantedAuthority role = (SecurityGrantedAuthority) iterator.next();
			if (role.getType().equals(type)) {
				roles.add(role);
			}
		}
		return roles;
	}

	public static GrantedAuthority getRole(String role) {
		for (GrantedAuthority authority : authorities) {
			if (authority.getAuthority().equals(role)) {
				return authority;
			}
		}
		return null;
	}

	public static GrantedAuthority getRoleByName(String name) {
		for (GrantedAuthority authority : authorities) {
			if (((SecurityGrantedAuthority) authority).getName().equals(name)) {
				return authority;
			}
		}
		return null;
	}

	public static boolean containsRole(String role) {
		for (GrantedAuthority authority : authorities) {
			if (authority.getAuthority().equals(role)) {
				return true;
			}
		}
		return false;
	}

	public static boolean containsName(String name) {
		for (GrantedAuthority authority : authorities) {
			if (((SecurityGrantedAuthority) authority).getName().equals(name)) {
				return true;
			}
		}
		return false;
	}

	public static boolean contains(GrantedAuthority authority, Authentication authentication) {
		if (authentication == null || authentication.getAuthorities() == null) {
			return false;
		}
		return authentication.getAuthorities().contains(authority);
	}

	public static boolean contains(GrantedAuthority authority, Principal principal) {
		if (principal == null) {
			return false;
		}

		Authentication authentication = (Authentication) principal;

		if (authentication.getAuthorities() == null) {
			return false;
		}
		return authentication.getAuthorities().contains(authority);
	}

	public static boolean hasRole(String role, org.springframework.security.core.userdetails.User principal) {
		if (principal == null) {
			return false;
		}
		if (principal.getAuthorities() == null) {
			return false;
		}
		return principal.getAuthorities().contains(getRole(role));
	}

	public static boolean hasRole(String role, User user) {
		if (user == null || user.getUserRoleList() == null) {
			return false;
		}
		for (UserRoles userRoles : user.getUserRoleList()) {
			if (userRoles.getRoleCode().equalsIgnoreCase(role)) {
				return true;
			}
		}
		return false;
	}
}
